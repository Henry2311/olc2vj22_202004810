
package olc2.proyecto1_202004810;

import Environment.*;
import Grammar.*;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Objects;
import java.util.Stack;

public class Visitor extends gramaticaBaseVisitor {

    Env preview;
    Stack<Env> pilaENV;
    String temp_type = "";
    public String consola;
    ArrayList<Symbol_error> errores = new ArrayList<>();
    public Visitor(Env env, Stack<Env> pilaENV) {
        consola = "";
        this.pilaENV = pilaENV;
        this.pilaENV.push(env);
        this.preview = env;
    }
    
    @Override
    public Object visitStart(gramaticaParser.StartContext ctx){
        return visit(ctx.sentencias());
    }
    
    
    @Override
    public Object visitSentencias(gramaticaParser.SentenciasContext ctx){
        for (gramaticaParser.SentenciaContext ictx : ctx.sentencia()){
            Object salida = visit(ictx);
            if(salida.equals("exit") | salida.equals("cycle")){
                return salida;
            }
        }
        return true; 
    }
    
    @Override
    public Object visitSentencia(gramaticaParser.SentenciaContext ctx){
        if(ctx.main()!=null){
            visitMain(ctx.main());
        }else if(ctx.declaracion()!=null){
            visitDeclaracion(ctx.declaracion());
        }else if(ctx.sentencia_if()!=null){
            Object salida = visitSentencia_if(ctx.sentencia_if());
            if(salida.equals("exit") | salida.equals("cycle")){
                return salida;
            }
        }else if(ctx.sentencia_do()!=null){
            visitSentencia_do(ctx.sentencia_do());
        }else if(ctx.sentencia_while()!=null){
            visitSentencia_while(ctx.sentencia_while());
        }else if(ctx.darreglo()!=null){
            visitDarreglo(ctx.darreglo());
        }else if(ctx.dmatriz()!=null){
            visitDmatriz(ctx.dmatriz());
        }else if(ctx.dinamico()!=null){
            visit(ctx.dinamico());
        }else if(ctx.asignacion()!=null){
            visit(ctx.asignacion());
        }else if(ctx.print()!=null){
            visitPrint(ctx.print());
        }else if(ctx.subrutina()!=null){
            visitSubrutina(ctx.subrutina());
        }else if(ctx.callback()!=null){
            visitCallback(ctx.callback());
        }else if(ctx.funcion()!=null){
            visitFuncion(ctx.funcion());
        }
        
        return true;
    }
    
    @Override
    public Object visitMain(gramaticaParser.MainContext ctx){
        if(ctx.id1.getText().equals(ctx.id2.getText())){
            Env e = new Env(pilaENV.peek());
            pilaENV.peek().next = e;
            pilaENV.push(e);
            visit(ctx.sentencias());
            pilaENV.pop();
        }else{
            errores.add(new Symbol_error( "Semantico","Los identificadores deben ser iguales: "+ctx.id1.getText()+", "+ctx.id2.getText(),ctx.ID(0).getSymbol().getLine(), ctx.ID(0).getSymbol().getCharPositionInLine()));
        }
        return true;
    }
    
    @Override
    public Object visitSubrutina(gramaticaParser.SubrutinaContext ctx){
        if(ctx.id1.getText().equals(ctx.id2.getText())){
            Env e = pilaENV.peek();
            boolean confirm = false;
            if(!e.tablaSimbolos.containsKey(ctx.ID(0).getText().toUpperCase())){
                ArrayList<Symbol> parametros = new ArrayList<>();
                int pos = 0;
                for(gramaticaParser.ExpContext paramE : ctx.params().exp()){
                    parametros.add(new Symbol(ctx.id1.getText(),"param",paramE.getText(),pos,0,0));
                    pos++;
                }
                Symbol_routine nuevo = new Symbol_routine(ctx.id1.getText(),parametros,ctx.sentencias(),ctx.decparams());
                e.new_symbol(ctx.id1.getText(), new Symbol(ctx.id1.getText(),"Sub",nuevo,e.getLast_position(),ctx.id1.getLine(),ctx.id1.getCharPositionInLine()));
                e.setLast_position(e.getLast_position()+1);
                confirm = true;
            }else{
                errores.add(new Symbol_error( "Semantico","La subrutina ya existe: "+ctx.ID(0).getText(),ctx.ID(0).getSymbol().getLine(), ctx.ID(0).getSymbol().getCharPositionInLine()));
            }
            if(confirm)System.out.println("Se ha guardado la subrutina");
        }else{
            errores.add(new Symbol_error( "Semantico","Los identificadores deben ser iguales: "+ctx.id1.getText()+", "+ctx.id2.getText(),ctx.ID(0).getSymbol().getLine(), ctx.ID(0).getSymbol().getCharPositionInLine()));
        }
        return true;
    }
    
    @Override
    public Object visitFuncion(gramaticaParser.FuncionContext ctx){
        if(ctx.id1.getText().equals(ctx.id2.getText())){
            Env e = pilaENV.peek();
            boolean confirm = false;
            if(!e.tablaSimbolos.containsKey(ctx.ID(0).getText().toUpperCase())){
                ArrayList<Symbol> parametros = new ArrayList<>();
                int pos = 0;
                for(gramaticaParser.ExpContext paramE : ctx.params().exp()){
                    parametros.add(new Symbol(ctx.id1.getText(),"Fun",paramE.getText(),pos,0,0));
                    pos++;
                }
                Symbol_function nuevo = new Symbol_function(ctx.id1.getText(),parametros,ctx.sentencias(),ctx.decparams(),ctx.res);
                e.new_symbol(ctx.id1.getText(), new Symbol(ctx.id1.getText(),"Fun",nuevo,e.getLast_position(),ctx.id1.getLine(),ctx.id1.getCharPositionInLine()));
                e.setLast_position(e.getLast_position()+1);
                confirm = true;
            }else{
                errores.add(new Symbol_error( "Semantico","La funcion ya existe: "+ctx.ID(0).getText(),ctx.ID(0).getSymbol().getLine(), ctx.ID(0).getSymbol().getCharPositionInLine()));
            }
            if(confirm)System.out.println("Se ha guardado la funcion");
        }else{
            errores.add(new Symbol_error( "Semantico","Los identificadores deben ser iguales: "+ctx.id1.getText()+", "+ctx.id2.getText(),ctx.ID(0).getSymbol().getLine(), ctx.ID(0).getSymbol().getCharPositionInLine()));
        }
        return true;
    }
    
    @Override
    public Object visitCallfunction(gramaticaParser.CallfunctionContext ctx){
        Env e = pilaENV.peek();
        Symbol routine = e.search(ctx.ID().getText());
        Object retorno = null;
        if(routine!=null){
            Env new_e = new Env(e);
            Symbol_function sub = (Symbol_function)routine.getValue();
            int pos = 0;
            if(sub.params.size()==ctx.params().exp().size()){
                for (int i = 0; i < ctx.params().exp().size(); i++) {
                    Symbol temp = sub.params.get(i);
                    String temp_id = temp.getValue().toString();
                    Symbol nuevo = null;
                    if(sub.decParam.initparams(i).declaracion()!=null){
                        Object valor = visit(ctx.params().exp(i));
                        nuevo = new Symbol(ctx.ID().getText(),sub.decParam.initparams(i).declaracion().tipo().getText(),valor,pos,0,0);
                        pos++;
                    }else if(sub.decParam.initparams(i).dmatriz()!=null){
                        Object [][] valor = (Object[][]) visit(ctx.params().exp(i));
                        Object [][] n_valor = new Object[valor.length][valor[0].length];
                        for (int j = 0; j < valor.length; j++) {
                            System.arraycopy(valor[j], 0, n_valor[j], 0, valor[j].length);
                        }
                        nuevo = new Symbol(ctx.ID().getText(),sub.decParam.initparams(i).dmatriz().tipo().getText(),n_valor,0,0);
                    }else if(sub.decParam.initparams(i).darreglo()!=null){
                        Object [] valor = (Object[]) visit(ctx.params().exp(i));
                        Object [] n_valor = new Object[valor.length];
                        System.arraycopy(valor, 0, n_valor, 0, valor.length);
                        nuevo = new Symbol(ctx.ID().getText(),sub.decParam.initparams(i).darreglo().tipo().getText(),n_valor,0,0);
                    }
                    new_e.new_symbol(temp_id, nuevo);
                }
                new_e.setLast_position(sub.ent.getLast_position());
                pilaENV.push(new_e);
                visitSentencias((gramaticaParser.SentenciasContext) sub.sentencias);
                sub.ent = pilaENV.peek();
                retorno = visit(sub.res);
                pilaENV.pop();
            }else{
                errores.add(new Symbol_error( "Semantico","La cantidad de parametros no corresponde",ctx.ID().getSymbol().getLine(), ctx.ID().getSymbol().getCharPositionInLine()));
            }
        }else{
            errores.add(new Symbol_error( "Semantico","La funcion no ha sido declarada: "+ctx.ID().getText(),ctx.ID().getSymbol().getLine(), ctx.ID().getSymbol().getCharPositionInLine()));
        }
        return retorno;
    }
    
    @Override
    public Object visitCallback(gramaticaParser.CallbackContext ctx){
        Env e = pilaENV.peek();
        Symbol routine = e.search(ctx.ID().getText());
        if(routine!=null){
            Env new_e = new Env(e);
            Symbol_routine sub = (Symbol_routine)routine.getValue();
            if(sub.params.size()==ctx.params().exp().size()){
                int pos = 0;
                for (int i = 0; i < ctx.params().exp().size(); i++) {
                    Symbol temp = sub.params.get(i);
                    String temp_id = temp.getValue().toString();
                    Symbol nuevo = null;
                    if(sub.decParam.initparams(i).declaracion()!=null){
                        Object valor = visit(ctx.params().exp(i));
                        nuevo = new Symbol(ctx.ID().getText(),sub.decParam.initparams(i).declaracion().tipo().getText(),valor,pos,0,0);
                        pos++;
                    }else if(sub.decParam.initparams(i).dmatriz()!=null){
                        Object [][] valor = (Object[][]) visit(ctx.params().exp(i));
                        Object [][] n_valor = new Object[valor.length][valor[0].length];
                        for (int j = 0; j < valor.length; j++) {
                            System.arraycopy(valor[j], 0, n_valor[j], 0, valor[j].length);
                        }
                        nuevo = new Symbol(ctx.ID().getText(),sub.decParam.initparams(i).dmatriz().tipo().getText(),n_valor,0,0);
                    }else if(sub.decParam.initparams(i).darreglo()!=null){
                        Object[] valor = (Object[]) visit(ctx.params().exp(i));
                        Object [] n_valor = new Object[valor.length];
                        System.arraycopy(valor, 0, n_valor, 0, valor.length);
                        nuevo = new Symbol(ctx.ID().getText(),sub.decParam.initparams(i).darreglo().tipo().getText(),n_valor,0,0);
                    }
                    new_e.new_symbol(temp_id, nuevo);
                }
                new_e.setLast_position(sub.ent.getLast_position());
                pilaENV.push(new_e);
                visitSentencias((gramaticaParser.SentenciasContext) sub.sentencias);
                sub.ent = pilaENV.peek();
                pilaENV.pop();
            }else{
                errores.add(new Symbol_error( "Semantico","El número de parametos no corresponde",ctx.ID().getSymbol().getLine(), ctx.ID().getSymbol().getCharPositionInLine()));
            }
        }else{
            errores.add(new Symbol_error( "Semantico","La subrutina no ha sido declarada: "+ctx.ID().getText(),ctx.ID().getSymbol().getLine(), ctx.ID().getSymbol().getCharPositionInLine()));
        }
        return true;
    }
    
    @Override
    public Object visitSentencia_while(gramaticaParser.Sentencia_whileContext ctx){
        while((boolean)visit(ctx.exp())){
            Env aux_e = new Env(pilaENV.peek());
            pilaENV.peek().next = aux_e;
            pilaENV.push(aux_e);
            Object salida = visit(ctx.sentencias());
            pilaENV.pop();
            if(salida.equals("exit")){
                break;
            }else if(salida.equals("cycle")){
                continue;
            }
        }

        return true;
    }
    
    @Override
    public Object visitSentencia_do(gramaticaParser.Sentencia_doContext ctx){
        int sup = (int) visit(ctx.sup),
            inc = (int) visit(ctx.asc);
        String ID = ctx.asignacion().children.get(0).getText();
        visit(ctx.asignacion());

            Env e = pilaENV.peek();
            Symbol var = e.search(ID);
            while((int) var.getValue() <= sup){
                Env aux_e = new Env(pilaENV.peek());
                pilaENV.peek().next = aux_e;
                pilaENV.push(aux_e);
                Object salida = visit(ctx.sentencias());
                e.update_symbol(ID, new Symbol(ID,var.getType(),((int) var.getValue())+inc,e.getLast_position(),var.getLine(),var.getColumn()));
                var = e.search(ID);
                pilaENV.pop();
                if(salida.equals("exit"))break;
                else if(salida.equals("cycle"))continue;
            }
        return true;
    }
    @Override
    public Object visitSentencia_if(gramaticaParser.Sentencia_ifContext ctx){
        if((boolean)visit(ctx.exp())){
            Env e = new Env(pilaENV.peek());
            pilaENV.peek().next = e;
            pilaENV.push(e);
            visit(ctx.sentencias());
            pilaENV.pop();
            if(ctx.salida().size()>0){
                return visit(ctx.salida(0));
            }
        }else{
            Object elif = true;
            if(ctx.sen_elseif().size() > 0){
                for (int i = 0; i < ctx.sen_elseif().size(); i++) {
                    elif = visit(ctx.sen_elseif(i));
                    if(!(boolean) elif)break;
                    if(elif.equals("exit") | elif.equals("cycle"))return elif;
                }
            }
            if((boolean)elif && ctx.sen_else().size()>0){
                for (int i = 0; i < ctx.sen_else().size(); i++) {
                    elif = visit(ctx.sen_else(i));
                    if(elif.equals("exit") | elif.equals("cycle"))return elif;
                }
            }
        }
        return true;
    }
    
    @Override
    public Object visitSalida(gramaticaParser.SalidaContext ctx){
        return ctx.getText();
    }
    
    @Override
    public Object visitSen_else(gramaticaParser.Sen_elseContext ctx){
        pilaENV.push(new Env(pilaENV.peek()));
        visit(ctx.sentencias());
        pilaENV.pop();
        if(ctx.salida().size()>0){
            return visit(ctx.salida(0));
        }
        return true;
    }
    
    @Override
    public Object visitSen_elseif(gramaticaParser.Sen_elseifContext ctx){
        if((boolean)visit(ctx.exp())){
            Env e = new Env(pilaENV.peek());
            pilaENV.peek().next = e;
            pilaENV.push(e);
            visit(ctx.sentencias());
            pilaENV.pop();
            if(ctx.salida().size()>0){
                return visit(ctx.salida(0));
            }
            return false;
        }
        return true;
    }
    
    @Override
    public Object visitPrint(gramaticaParser.PrintContext ctx){
        String print = "";
        for (int i = 0; i < ctx.exp().size(); i++) {
            if(visit(ctx.exp(i)).getClass().equals(Object[].class)){
                Object t [] = (Object[]) visit(ctx.exp(i));
                String arr = "[";
                for (int j = 0; j < t.length; j++) {
                    arr+=t[j].toString()+",";
                }
                arr = arr.substring(0, arr.length()-1);
                print+=arr+"] ";

            }else if(visit(ctx.exp(i)).getClass().equals(Object[][].class)){
                Object t [][] = (Object[][]) visit(ctx.exp(i));
                String arr = "[";
                for (int j = 0; j < t.length; j++) {
                    arr+="[";
                    for (int k = 0; k < t[j].length; k++) {
                        arr+=t[j][k]+",";
                    }
                    arr = arr.substring(0, arr.length()-1);
                    arr += "],\n";
                }
                arr = arr.substring(0, arr.length()-2);
                print+=arr+"] ";
            }else{
                print+=visit(ctx.exp(i))+" ";
            }
            
        }
        consola += print+"\n";
        return true;
    }
    
    @Override
    public Object visitResetD(gramaticaParser.ResetDContext ctx){
        
        Env e = pilaENV.peek();
        if(e.tablaSimbolos.containsKey(ctx.ID().getText().toUpperCase())){
            Symbol nuevo = null;
            Symbol var = e.search(ctx.ID().getText());
            if(var.getValue()!=null){
                errores.add(new Symbol_error( "Semantico","No es un arreglo dinamico: "+ctx.ID().getText(),ctx.ID().getSymbol().getLine(), ctx.ID().getSymbol().getCharPositionInLine()));
            }else if(var.getValue_mtx()!=null){
                Object valor [][] = null;
                nuevo = new Symbol(ctx.ID().getText(),var.getType(), valor,var.getLine(),var.getColumn());
            }else if(var.getValue_arr()!=null){
                Object valor [] = null;
                nuevo = new Symbol(ctx.ID().getText(),var.getType(), valor,var.getLine(),var.getColumn());
            }
            e.new_symbol(ctx.ID().getText(), nuevo);
        }else{
            errores.add(new Symbol_error( "Semantico","La variable no existe: "+ctx.ID().getText(),ctx.ID().getSymbol().getLine(), ctx.ID().getSymbol().getCharPositionInLine()));
        }
        return true;
    }
    
    @Override
    public Object visitTvector(gramaticaParser.TvectorContext ctx){
        Env e = pilaENV.peek();
        if(e.exist(ctx.ID().getText())){
            Symbol var = e.search(ctx.ID().getText());
            Object valor [] = new Object[(int) visit(ctx.exp())];
            switch(var.getType()){
                case "integer":
                    Arrays.fill(valor, 0);
                    break;
                case "real" :
                    Arrays.fill(valor, 0.0);
                    break;
                case "logical" :
                    Arrays.fill(valor, false);
                    break;
                case "complex" : //valor = "(9.192517926E-43,0.0)";
                    Arrays.fill(valor, "(9.192517926E-43,0.0)");
                    break;
                case "character" :
                    Arrays.fill(valor, ' ');
                    break;
            }
            Symbol nuevo = new Symbol(ctx.ID().getText(),var.getType(), valor,var.getLine(),var.getColumn());
            e.update_symbol(ctx.ID().getText(), nuevo);
        }else{
            errores.add(new Symbol_error( "Semantico","La variable no existe: "+ctx.ID().getText(),ctx.ID().getSymbol().getLine(), ctx.ID().getSymbol().getCharPositionInLine()));
        }
        return true;
    }
    
    @Override
    public Object visitTmatriz(gramaticaParser.TmatrizContext ctx){
        Env e = pilaENV.peek();
        if(e.exist(ctx.ID().getText())){
            Symbol var = e.search(ctx.ID().getText());
            Object valor [][] = new Object[(int) visit(ctx.x)][(int) visit(ctx.y)];
            switch(var.getType()){
                case "integer": 
                    for (Object[] valor1 : valor) {
                        Arrays.fill(valor1, 0);
                    }
                    break;
                case "real" :
                    for (Object[] valor1 : valor) {
                        Arrays.fill(valor1, 0.0);
                    }
                    break;
                case "logical" : 
                    for (Object[] valor1 : valor) {
                        Arrays.fill(valor1, false);
                    }
                    break;
                case "complex" : //valor = "(9.192517926E-43,0.0)"; 
                    for (Object[] valor1 : valor) {
                        Arrays.fill(valor1, "(9.192517926E-43,0.0)");
                    }
                    break;
                case "character" : 
                    for (Object[] valor1 : valor) {
                        Arrays.fill(valor1, ' ');
                    }
                    break;
            }
            Symbol nuevo = new Symbol(ctx.ID().getText(),var.getType(), valor,var.getLine(),var.getColumn());
            e.update_symbol(ctx.ID().getText(), nuevo);
        }else{
            errores.add(new Symbol_error( "Semantico","La variable no existe: "+ctx.ID().getText(),ctx.ID().getSymbol().getLine(), ctx.ID().getSymbol().getCharPositionInLine()));
        }
        return true;
    }
    
    @Override
    public Object visitDinamicoV(gramaticaParser.DinamicoVContext ctx){
        Env e = pilaENV.peek();
        if(!e.tablaSimbolos.containsKey(ctx.ID().getText().toUpperCase())){
            Object valor []=null;
            Symbol nuevo = new Symbol(ctx.ID().getText(),ctx.tipo().getText(), valor,ctx.ID().getSymbol().getLine(),ctx.ID().getSymbol().getCharPositionInLine());
            e.new_symbol(ctx.ID().getText(), nuevo);
        }else{
            errores.add(new Symbol_error( "Semantico","La variable ya existe: "+ctx.ID().getText(),ctx.ID().getSymbol().getLine(), ctx.ID().getSymbol().getCharPositionInLine()));
        }
        return true;
    }
    
    @Override
    public Object visitDinamicoM(gramaticaParser.DinamicoMContext ctx){
        Env e = pilaENV.peek();
        if(!e.tablaSimbolos.containsKey(ctx.ID().getText().toUpperCase())){
            Object valor [][]=null;
            Symbol nuevo = new Symbol(ctx.ID().getText(),ctx.tipo().getText(), valor,ctx.ID().getSymbol().getLine(),ctx.ID().getSymbol().getCharPositionInLine());
            e.new_symbol(ctx.ID().getText(), nuevo);
        }else{
            errores.add(new Symbol_error( "Semantico","La variable ya existe: "+ctx.ID().getText(),ctx.ID().getSymbol().getLine(), ctx.ID().getSymbol().getCharPositionInLine()));
        }
        return true;
    }
    
    @Override
    public Object visitDmatriz(gramaticaParser.DmatrizContext ctx){
        Env e = pilaENV.peek();
        if(!e.tablaSimbolos.containsKey(ctx.ID().getText().toUpperCase())){
            Object valor [][]=null;
            int x = (int) visit(ctx.x), y = (int) visit(ctx.y);
            switch(ctx.tipo().getText()){
                case "integer": 
                    valor = new Object[x][y];
                    for (Object[] valor1 : valor) {
                        for (int j = 0; j < valor1.length; j++) {
                            valor1[j] = 0;
                        }
                    }
                    break;
                case "real" :
                    valor = new Object[x][y];
                    for (Object[] valor1 : valor) {
                        for (int j = 0; j < valor1.length; j++) {
                            valor1[j] = 0.0;
                        }
                    }
                    break;
                case "logical" : 
                    valor = new Object[x][y];
                    for (Object[] valor1 : valor) {
                        for (int j = 0; j < valor1.length; j++) {
                            valor1[j] = false;
                        }
                    }
                    break;
                case "complex" : //valor = "(9.192517926E-43,0.0)"; 
                    valor = new Object[x][y];
                    for (Object[] valor1 : valor) {
                        for (int j = 0; j < valor1.length; j++) {
                            valor1[j] = "(9.192517926E-43,0.0)";
                        }
                    }
                    break;
                case "character" : 
                    valor = new Object[x][y];
                    for (Object[] valor1 : valor) {
                        for (int j = 0; j < valor1.length; j++) {
                            valor1[j] = ' ';
                        }
                    }
                    break;
            }
            Symbol nuevo = new Symbol(ctx.ID().getText(),ctx.tipo().getText(), valor,ctx.ID().getSymbol().getLine(),ctx.ID().getSymbol().getCharPositionInLine());
            e.new_symbol(ctx.ID().getText(), nuevo);
        }else{
            errores.add(new Symbol_error( "Semantico","La variable ya existe: "+ctx.ID().getText(),ctx.ID().getSymbol().getLine(), ctx.ID().getSymbol().getCharPositionInLine()));
        }
        return true;
    }
    
    @Override
    public Object visitDarreglo(gramaticaParser.DarregloContext ctx){
        Env e = pilaENV.peek();
        if(!e.tablaSimbolos.containsKey(ctx.ID().getText().toUpperCase())){
            Object valor []=null;
            int index = (int) visit(ctx.exp());
            switch(ctx.tipo().getText()){
                case "integer": 
                    valor = new Object[index];
                    for (int i = 0; i < valor.length; i++) {
                        valor[i]=0;
                    }
                    break;
                case "real" :
                    valor = new Object[index];
                    for (int i = 0; i < valor.length; i++) {
                        valor[i]=0.0;
                    }
                    break;
                case "logical" : 
                    valor = new Object[index];
                    for (int i = 0; i < valor.length; i++) {
                        valor[i]=false;
                    }
                    break;
                case "complex" : //valor = "(9.192517926E-43,0.0)"; 
                    valor = new Object[index];
                    for (int i = 0; i < valor.length; i++) {
                        valor[i]="(9.192517926E-43,0.0)";
                    }
                    break;
                case "character" : 
                    valor = new Object[index];
                    for (int i = 0; i < valor.length; i++) {
                        valor[i]=' ';
                    }
                    break;
            }
            Symbol nuevo = new Symbol(ctx.ID().getText(),ctx.tipo().getText(), valor,ctx.ID().getSymbol().getLine(),ctx.ID().getSymbol().getCharPositionInLine());
            e.new_symbol(ctx.ID().getText(), nuevo);
        }else{
            errores.add(new Symbol_error( "Semantico","La variable ya existe: "+ctx.ID().getText(),ctx.ID().getSymbol().getLine(), ctx.ID().getSymbol().getCharPositionInLine()));
        }
        return true;
    }
    
    @Override
    public Object visitAvlista(gramaticaParser.AvlistaContext ctx){
        Env e = pilaENV.peek();
        if(e.exist(ctx.ID().getText())){
            Symbol var = e.search(ctx.ID().getText());
            Object arr [] = var.getValue_arr();
            if(arr.length == ctx.exp().size()){
                boolean s = false;
                for (int i = 0; i < ctx.exp().size(); i++) {
                    if(arr[i].getClass().equals(visit(ctx.exp(i)).getClass())){
                        arr[i] = visit(ctx.exp(i));
                        s=true;
                    }else{
                        s=false;
                        break;
                    }
                }
                if(s){
                    Symbol nuevo = new Symbol(ctx.ID().getText(),var.getType(),arr,var.getLine(),var.getColumn());
                    e.update_symbol(ctx.ID().getText(), nuevo);
                }else{
                    errores.add(new Symbol_error( "Semantico","Tipo de dato incompatible",ctx.ID().getSymbol().getLine(), ctx.ID().getSymbol().getCharPositionInLine()));
                }
            }else{
                errores.add(new Symbol_error( "Semantico","La cantidad de datos no es soportada por el arreglo",ctx.ID().getSymbol().getLine(), ctx.ID().getSymbol().getCharPositionInLine()));
            }
        }else{
            errores.add(new Symbol_error( "Semantico","La variable ya existe: "+ctx.ID().getText(),ctx.ID().getSymbol().getLine(), ctx.ID().getSymbol().getCharPositionInLine()));
        }
        return true;
    }
    
    @Override
    public Object visitAmatriz(gramaticaParser.AmatrizContext ctx){
        Env e = pilaENV.peek();
        if(e.exist(ctx.ID().getText())){
            Symbol var = e.search(ctx.ID().getText());
            int x = (int) visit(ctx.x), y = (int) visit(ctx.y);
            Object temp [][] = var.getValue_mtx();
            if(temp[x-1][y-1].getClass().equals(visit(ctx.valor).getClass())){
                temp[x-1][y-1] = visit(ctx.valor);
                Symbol nuevo = new Symbol(ctx.ID().getText(),var.getType(),temp,var.getLine(),var.getColumn());
                e.update_symbol(ctx.ID().getText(), nuevo);
            }else{
                errores.add(new Symbol_error( "Semantico","Tipo de dato incompatible",ctx.ID().getSymbol().getLine(), ctx.ID().getSymbol().getCharPositionInLine()));
            }
        }else{
            errores.add(new Symbol_error( "Semantico","La variable ya existe: "+ctx.ID().getText(),ctx.ID().getSymbol().getLine(), ctx.ID().getSymbol().getCharPositionInLine()));
        }
        return true;
    }
    
    @Override
    public Object visitAvector(gramaticaParser.AvectorContext ctx){
        Env e = pilaENV.peek();
        if(e.exist(ctx.ID().getText())){
            Symbol var = e.search(ctx.ID().getText());
            int index = (int) visit(ctx.index);
            Object temp [] = var.getValue_arr();
            if(temp[index-1].getClass().equals(visit(ctx.valor).getClass())){
                temp[index-1] = visit(ctx.valor);
                Symbol nuevo = new Symbol(ctx.ID().getText(),var.getType(),temp,var.getLine(),var.getColumn());
                e.update_symbol(ctx.ID().getText(), nuevo);
            }else{
                errores.add(new Symbol_error( "Semantico","Tipo de dato incompatible",ctx.ID().getSymbol().getLine(), ctx.ID().getSymbol().getCharPositionInLine()));
            }
        }else{
            errores.add(new Symbol_error( "Semantico","La variable ya existe: "+ctx.ID().getText(),ctx.ID().getSymbol().getLine(), ctx.ID().getSymbol().getCharPositionInLine()));
        }
        return true;
    }
    
    @Override
    public Object visitAvariable(gramaticaParser.AvariableContext ctx){
        Env e = pilaENV.peek();
        if(e.exist(ctx.ID().getText())){
            Symbol var = e.search(ctx.ID().getText());
            if(var.getValue() != null){
                if(visit(ctx.exp()).getClass().equals(var.getValue().getClass())){
                    Symbol nuevo = new Symbol(ctx.ID().getText(),var.getType(),visit(ctx.exp()),var.getPosition(),var.getLine(),var.getColumn());
                    e.update_symbol(ctx.ID().getText(), nuevo);
                }else{
                    errores.add(new Symbol_error( "Semantico","Tipo de dato incompatible",ctx.ID().getSymbol().getLine(), ctx.ID().getSymbol().getCharPositionInLine()));
                }
            }else if(var.getValue_arr()!=null){
                if(visit(ctx.exp()).getClass().equals(var.getValue_arr().getClass())){
                    Object [] temp_Arr = (Object[]) visit(ctx.exp());
                    if(temp_Arr.length==var.getValue_arr().length){
                        Symbol nuevo = new Symbol(ctx.ID().getText(),var.getType(), (Object[]) visit(ctx.exp()),var.getLine(),var.getColumn());
                        e.update_symbol(ctx.ID().getText(), nuevo);
                    }else{
                        errores.add(new Symbol_error( "Semantico","El tamaño es incompatible",ctx.ID().getSymbol().getLine(), ctx.ID().getSymbol().getCharPositionInLine()));
                    }
                }else{
                    errores.add(new Symbol_error( "Semantico","Tipo de dato incompatible",ctx.ID().getSymbol().getLine(), ctx.ID().getSymbol().getCharPositionInLine()));
                }
            }else if(var.getValue_mtx()!=null){
                if(visit(ctx.exp()).getClass().equals(var.getValue_arr().getClass())){
                    Object [][] temp_Arr = (Object[][]) visit(ctx.exp());
                    if(temp_Arr.length==var.getValue_mtx().length && temp_Arr[0].length == var.getValue_mtx()[0].length){
                        Symbol nuevo = new Symbol(ctx.ID().getText(),var.getType(), (Object[]) visit(ctx.exp()),var.getLine(),var.getColumn());
                        e.update_symbol(ctx.ID().getText(), nuevo);
                    }else{
                        errores.add(new Symbol_error( "Semantico","El tamaño es incompatible",ctx.ID().getSymbol().getLine(), ctx.ID().getSymbol().getCharPositionInLine()));
                    }
                }else{
                    errores.add(new Symbol_error( "Semantico","Tipo de dato incompatible",ctx.ID().getSymbol().getLine(), ctx.ID().getSymbol().getCharPositionInLine()));
                }
            }
        }else{
            errores.add(new Symbol_error( "Semantico","La variable ya existe: "+ctx.ID().getText(),ctx.ID().getSymbol().getLine(), ctx.ID().getSymbol().getCharPositionInLine()));
        }
        return true;
    }
    
    @Override
    public Object visitDeclaracion(gramaticaParser.DeclaracionContext ctx){
        temp_type = ctx.tipo().getText();
        return visit(ctx.ldeclaraciones());
    }
    
    @Override
    public Object visitDindv(gramaticaParser.DindvContext ctx){
        Env e = pilaENV.peek();
        if(!e.tablaSimbolos.containsKey(ctx.ID().getText().toUpperCase())){
            Object valor = switch (temp_type) {
                case "integer" -> 0;
                case "real" -> 0.0;
                case "logical" -> false;
                case "complex" -> "(9.192517926E-43,0.0)";
                case "character" -> ' ';
                default -> null;
            };
            Symbol nuevo = new Symbol(ctx.ID().getText(),temp_type, valor,e.getLast_position(),ctx.ID().getSymbol().getLine(),ctx.ID().getSymbol().getCharPositionInLine());
            e.setLast_position(e.getLast_position()+1);
            e.new_symbol(ctx.ID().getText(), nuevo);
        }else{
            errores.add(new Symbol_error( "Semantico","La variable ya existe: "+ctx.ID().getText(),ctx.ID().getSymbol().getLine(), ctx.ID().getSymbol().getCharPositionInLine()));
        }
        return visit(ctx.ldeclaraciones());
    }
    
    @Override
    public Object visitDvalor(gramaticaParser.DvalorContext ctx){
        Env e = pilaENV.peek();
        if(!e.tablaSimbolos.containsKey(ctx.ID().getText().toUpperCase())){
            Symbol nuevo;
            if(temp_type.equals("integer") && visit(ctx.exp()).getClass().equals(Integer.class)){
                nuevo = new Symbol(ctx.ID().getText(),temp_type, visit(ctx.exp()),e.getLast_position(),ctx.ID().getSymbol().getLine(),ctx.ID().getSymbol().getCharPositionInLine());
                e.setLast_position(e.getLast_position()+1);
                e.new_symbol(ctx.ID().getText(), nuevo);
            }else if(temp_type.equals("real") && visit(ctx.exp()).getClass().equals(Double.class)){
                nuevo = new Symbol(ctx.ID().getText(),temp_type, visit(ctx.exp()),e.getLast_position(),ctx.ID().getSymbol().getLine(),ctx.ID().getSymbol().getCharPositionInLine());
                e.setLast_position(e.getLast_position()+1);
                e.new_symbol(ctx.ID().getText(), nuevo);
            }else if(temp_type.equals("character") && visit(ctx.exp()).getClass().equals(Character.class)){
                nuevo = new Symbol(ctx.ID().getText(),temp_type, visit(ctx.exp()),e.getLast_position(),ctx.ID().getSymbol().getLine(),ctx.ID().getSymbol().getCharPositionInLine());
                e.setLast_position(e.getLast_position()+1);
                e.new_symbol(ctx.ID().getText(), nuevo);
            }else if(temp_type.equals("logical") && visit(ctx.exp()).getClass().equals(Boolean.class)){
                nuevo = new Symbol(ctx.ID().getText(),temp_type, visit(ctx.exp()),e.getLast_position(),ctx.ID().getSymbol().getLine(),ctx.ID().getSymbol().getCharPositionInLine());
                e.setLast_position(e.getLast_position()+1);
                e.new_symbol(ctx.ID().getText(), nuevo);
            }else{
                errores.add(new Symbol_error( "Semantico","No es el tipo de dato correcto",ctx.ID().getSymbol().getLine(), ctx.ID().getSymbol().getCharPositionInLine()));
            }
        }else{
            errores.add(new Symbol_error( "Semantico","La variable ya existe: "+ctx.ID().getText(),ctx.ID().getSymbol().getLine(), ctx.ID().getSymbol().getCharPositionInLine()));
        }
        return visit(ctx.ldeclaraciones());
    }
    
    @Override
    public Object visitDvalor2(gramaticaParser.Dvalor2Context ctx){
        Env e = pilaENV.peek();
        if(!e.tablaSimbolos.containsKey(ctx.ID().getText().toUpperCase())){
            Symbol nuevo;
            if(temp_type.equals("integer") && visit(ctx.exp()).getClass().equals(Integer.class)){
                nuevo = new Symbol(ctx.ID().getText(),temp_type, visit(ctx.exp()),e.getLast_position(),ctx.ID().getSymbol().getLine(),ctx.ID().getSymbol().getCharPositionInLine());
                e.setLast_position(e.getLast_position()+1);
                e.new_symbol(ctx.ID().getText(), nuevo);
            }else if(temp_type.equals("real") && visit(ctx.exp()).getClass().equals(Double.class)){
                nuevo = new Symbol(ctx.ID().getText(),temp_type, visit(ctx.exp()),e.getLast_position(),ctx.ID().getSymbol().getLine(),ctx.ID().getSymbol().getCharPositionInLine());
                e.setLast_position(e.getLast_position()+1);
                e.new_symbol(ctx.ID().getText(), nuevo);
            }else if(temp_type.equals("character") && visit(ctx.exp()).getClass().equals(Character.class)){
                nuevo = new Symbol(ctx.ID().getText(),temp_type, visit(ctx.exp()),e.getLast_position(),ctx.ID().getSymbol().getLine(),ctx.ID().getSymbol().getCharPositionInLine());
                e.setLast_position(e.getLast_position()+1);
                e.new_symbol(ctx.ID().getText(), nuevo);
            }else if(temp_type.equals("logical") && visit(ctx.exp()).getClass().equals(Boolean.class)){
                nuevo = new Symbol(ctx.ID().getText(),temp_type, visit(ctx.exp()),e.getLast_position(),ctx.ID().getSymbol().getLine(),ctx.ID().getSymbol().getCharPositionInLine());
                e.setLast_position(e.getLast_position()+1);
                e.new_symbol(ctx.ID().getText(), nuevo);
            }else{
                errores.add(new Symbol_error( "Semantico","No es el tipo de dato correcto",ctx.ID().getSymbol().getLine(), ctx.ID().getSymbol().getCharPositionInLine()));
            }
        }else{
            errores.add(new Symbol_error( "Semantico","La variable ya existe: "+ctx.ID().getText(),ctx.ID().getSymbol().getLine(), ctx.ID().getSymbol().getCharPositionInLine()));
        }
        temp_type = "";
        return true;
    }
    
    @Override
    public Object visitDindv2(gramaticaParser.Dindv2Context ctx){
        Env e = pilaENV.peek();
        if(!e.tablaSimbolos.containsKey(ctx.ID().getText().toUpperCase())){
            Object valor = switch (temp_type) {
                case "integer" -> 0;
                case "real" -> 0.0;
                case "logical" -> false;
                case "complex" -> "(9.192517926E-43,0.0)";
                case "character" -> ' ';
                default -> null;
            };
            Symbol nuevo = new Symbol(ctx.ID().getText(),temp_type, valor,e.getLast_position(),ctx.ID().getSymbol().getLine(),ctx.ID().getSymbol().getCharPositionInLine());
            e.setLast_position(e.getLast_position()+1);
            e.new_symbol(ctx.ID().getText(), nuevo);
        }else{
            errores.add(new Symbol_error( "Semantico","La variable ya existe: "+ctx.ID().getText(),ctx.ID().getSymbol().getLine(), ctx.ID().getSymbol().getCharPositionInLine()));
        }
        temp_type = "";
        return true;
    }
    
    @Override
    public Object visitOpExpr(gramaticaParser.OpExprContext ctx){
        
        String operacion = ctx.op.getText();
        if(visit(ctx.left).getClass().equals(Integer.class) && visit(ctx.right).getClass().equals(Integer.class)){
            int izq = (int)visit(ctx.left);
            int der = (int)visit(ctx.right);
            
            switch (operacion) {
                case "==":
                case ".eq.":
                    return izq == der;
                case "/=":
                case ".ne.":
                    return izq!=der;
                case ">":
                case ".gt.":
                    return izq>der;
                case "<":
                case ".lt.":
                    return izq<der;
                case ">=":
                case ".ge.":
                    return izq>=der;
                case"<=":
                case ".le.":
                    return izq<=der;
                default:
                    break;
            }
            
        }else if(visit(ctx.left).getClass().equals(Double.class) && visit(ctx.right).getClass().equals(Double.class)){
            double izq = (double)visit(ctx.left);
            double der = (double)visit(ctx.right);
            
            switch (operacion) {
                case "==":
                case ".eq.":
                    return izq == der;
                case "/=":
                case ".ne.":
                    return izq!=der;
                case ">":
                case ".gt.":
                    return izq>der;
                case "<":
                case ".lt.":
                    return izq<der;
                case ">=":
                case ".ge.":
                    return izq>=der;
                case"<=":
                case ".le.":
                    return izq<=der;
                default:
                    break;
            }
        }else{
            errores.add(new Symbol_error( "Semantico","Tipo de datos invalido ",ctx.op.getLine(), ctx.op.getCharPositionInLine()));
        }
        
        return errores.add(new Symbol_error( "Semantico","Operacoin no valida: "+ctx.op,ctx.op.getLine(), ctx.op.getCharPositionInLine()));

    }

    @Override
    public Object visitOpExp(gramaticaParser.OpExpContext ctx){
        String operacion = ctx.op.getText();
        if(visit(ctx.left).getClass().equals(Integer.class) && visit(ctx.right).getClass().equals(Integer.class)){
            int izq = (int)visit(ctx.left);
            int der = (int)visit(ctx.right);
            if(der == 0 && operacion.equals("/")){
                errores.add(new Symbol_error( "Semantico","Error matematico division por 0",ctx.op.getLine(), ctx.op.getCharPositionInLine()));
                return  null;
            }
            switch (operacion) {
                case "**": return (int) Math.pow(izq, der);
                case "*" : return izq * der;
                case "/" : return (int) Math.floor(izq / der);
                case "+" : return izq + der;
                case "-" : return izq - der;
                default: errores.add(new Symbol_error( "Semantico","Operacion no valida: "+ctx.op,ctx.op.getLine(), ctx.op.getCharPositionInLine()));
            }
        }else if(visit(ctx.left).getClass().equals(Double.class) && visit(ctx.right).getClass().equals(Double.class)){
            double izq = (double)visit(ctx.left);
            double der = (double)visit(ctx.right);
            if(der == 0 && operacion.equals("/")){
                errores.add(new Symbol_error( "Semantico","Error matematico division por 0",ctx.op.getLine(), ctx.op.getCharPositionInLine()));
                return  null;
            }
            switch (operacion) {
                case "**": return Math.pow(izq, der);
                case "*" : return izq * der;
                case "/" : return izq / der;
                case "+" : return izq + der;
                case "-" : return izq - der;
                default: errores.add(new Symbol_error( "Semantico","Operacion no valida: "+ctx.op,ctx.op.getLine(), ctx.op.getCharPositionInLine()));
            }
        }else if(visit(ctx.left).getClass().equals(Double.class) && visit(ctx.right).getClass().equals(Integer.class)){
            double izq = (double)visit(ctx.left);
            double der = Double.parseDouble(visit(ctx.right).toString());
            if(der == 0 && operacion.equals("/")){
                errores.add(new Symbol_error( "Semantico","Error matematico division por 0",ctx.op.getLine(), ctx.op.getCharPositionInLine()));
                return  null;
            }
            switch (operacion) {
                case "**": return Math.pow(izq, der);
                case "*" : return izq * der;
                case "/" : return izq / der;
                case "+" : return izq + der;
                case "-" : return izq - der;
                default: errores.add(new Symbol_error( "Semantico","Operacion no valida: "+ctx.op,ctx.op.getLine(), ctx.op.getCharPositionInLine()));
            }
        }else if(visit(ctx.left).getClass().equals(Integer.class) && visit(ctx.right).getClass().equals(Double.class)){
            double izq = Double.parseDouble(visit(ctx.left).toString());
            double der = (double)visit(ctx.right);
            if(der == 0 && operacion.equals("/")){
                errores.add(new Symbol_error( "Semantico","Error matematico division por 0",ctx.op.getLine(), ctx.op.getCharPositionInLine()));
                return  null;
            }
            switch (operacion) {
                case "**": return Math.pow(izq, der);
                case "*" : return izq * der;
                case "/" : return izq / der;
                case "+" : return izq + der;
                case "-" : return izq - der;
                default: errores.add(new Symbol_error( "Semantico","Operacion no valida: "+ctx.op,ctx.op.getLine(), ctx.op.getCharPositionInLine()));
            }
        }else{
            errores.add(new Symbol_error( "Semantico","Tipo de datos invalido ",ctx.op.getLine(), ctx.op.getCharPositionInLine()));
        }
        
        return errores.add(new Symbol_error( "Semantico","Operacion no valida: "+ctx.op,ctx.op.getLine(), ctx.op.getCharPositionInLine()));
    }
        
    @Override
    public Object visitOpExpl(gramaticaParser.OpExplContext ctx){
        String operacion = ctx.op.getText();
        boolean izq = (boolean)visit(ctx.left);
        boolean der = (boolean)visit(ctx.right);
            
        switch (operacion) {
            case ".and.":
                return izq&&der;
            case ".or.":
                return izq||der;
            default:
                break;
        }
        return errores.add(new Symbol_error( "Semantico","Operacion no valida: "+ctx.op,ctx.op.getLine(), ctx.op.getCharPositionInLine()));
    }
    
    @Override
    public Object visitOpExpNot(gramaticaParser.OpExpNotContext ctx){
        String operacion = ctx.op.getText();
        if(operacion.equals(".not.")){
            boolean der = (boolean) visit(ctx.right);
            return !der;
        }else errores.add(new Symbol_error( "Semantico","Operacion no valida: "+ctx.op,ctx.op.getLine(), ctx.op.getCharPositionInLine()));
        return null;
    }
    
    @Override
    public Object visitOpExpMenos(gramaticaParser.OpExpMenosContext ctx){
        if(visit(ctx.right).getClass().equals(Integer.class)){
            return -(int)visit(ctx.right);
        }else if(visit(ctx.right).getClass().equals(Double.class)){
            return -(double)visit(ctx.right);
        }else{
            errores.add(new Symbol_error( "Semantico","Tipo de datos invalido ",ctx.op.getLine(), ctx.op.getCharPositionInLine()));
        }
        return null;
    }

    @Override
    public Integer visitValorArr(gramaticaParser.ValorArrContext ctx){
        Env e = pilaENV.peek();
        int tamaño = 0;
        Symbol id = e.search(ctx.ID().getText());
        if (id == null) errores.add(new Symbol_error( "Semantico","La variable "+ctx.ID().getText()+" no ha sido declarada",ctx.ID().getSymbol().getLine(), ctx.ID().getSymbol().getCharPositionInLine()));
        else{
            if(id.getValue_arr()!=null){
                Object temp [] = id.getValue_arr();
                tamaño = temp.length;
            }else if(id.getValue_mtx()!=null){
                Object temp [][] = id.getValue_mtx();
                tamaño = temp.length*temp[0].length;
            }else{
                errores.add(new Symbol_error( "Semantico",ctx.ID().getText()+" No es un arreglo",ctx.ID().getSymbol().getLine(), ctx.ID().getSymbol().getCharPositionInLine()));
            }
        }
        return tamaño;
    }
    @Override
    public Object visitLiteralID(gramaticaParser.LiteralIDContext ctx){
        Env e = pilaENV.peek();
        Symbol id = e.search(ctx.ID().getText());
        if (id == null) errores.add(new Symbol_error( "Semantico","La variable "+ctx.ID().getText()+" no ha sido declarada",ctx.ID().getSymbol().getLine(), ctx.ID().getSymbol().getCharPositionInLine()));
        else{
            if(id.getValue_arr()!=null){
                Object[] temp = id.getValue_arr();
                return temp;
            }else if(id.getValue_mtx()!=null){
                Object[][] temp = id.getValue_mtx();
                return temp;
            }else{
                return id.getValue();
            }
        }
        return null;
    }
    
    @Override
    public Object visitLiteralArr(gramaticaParser.LiteralArrContext ctx){
        Env e = pilaENV.peek();
        Symbol id = e.search(ctx.ID().getText());
        Object temp [] = id.getValue_arr();
        if (id == null) errores.add(new Symbol_error( "Semantico","El arreglo "+ctx.ID().getText()+" no ha sido declarada",ctx.ID().getSymbol().getLine(), ctx.ID().getSymbol().getCharPositionInLine()));
        else if(temp==null){
            errores.add(new Symbol_error( "Semantico","El arreglo "+ctx.ID().getText()+" no tiene tamaño definido",ctx.ID().getSymbol().getLine(), ctx.ID().getSymbol().getCharPositionInLine()));
        }else{return temp[((int) visit(ctx.exp()))-1];}

        return null;
    }
    
    @Override
    public Object visitLiteralMtx(gramaticaParser.LiteralMtxContext ctx){
        Env e = pilaENV.peek();
        Symbol id = e.search(ctx.ID().getText());
        Object temp [][] = id.getValue_mtx();
        if (id == null) errores.add(new Symbol_error( "Semantico","El arreglo "+ctx.ID().getText()+" no ha sido declarada",ctx.ID().getSymbol().getLine(), ctx.ID().getSymbol().getCharPositionInLine()));

        else if(temp==null){
            errores.add(new Symbol_error( "Semantico","El arreglo "+ctx.ID().getText()+" no tiene tamaño definido",ctx.ID().getSymbol().getLine(), ctx.ID().getSymbol().getCharPositionInLine()));
        }else return temp[((int) visit(ctx.x))-1][((int) visit(ctx.y))-1];

        return null;
    }
    
    @Override
    public Object visitParenExp(gramaticaParser.ParenExpContext ctx){
        return visit(ctx.exp());
    }
    
    @Override
    public Integer visitLiteralINT(gramaticaParser.LiteralINTContext ctx){
        return Integer.parseInt(ctx.getText());
    }
    
    @Override
    public Character visitLiteralCHR(gramaticaParser.LiteralCHRContext ctx){
        return ctx.getText().charAt(1);
    }
    
    @Override
    public Boolean visitLiteralBOL(gramaticaParser.LiteralBOLContext ctx){
        switch(ctx.getText()){
            case ".true.": return true;
            case ".false.": return false;
            default: return false;
        }
    }
    
    @Override
    public Double visitLiteralDEC(gramaticaParser.LiteralDECContext ctx){
        return Double.parseDouble(ctx.getText());
    }
    
    @Override
    public String visitLiteralSTR(gramaticaParser.LiteralSTRContext ctx){
        String str = ctx.getText().replaceAll("\"", "");
        return str;
    }
    
    @Override
    public Object visitTipo(gramaticaParser.TipoContext ctx){
        return ctx.getText();
    }
}