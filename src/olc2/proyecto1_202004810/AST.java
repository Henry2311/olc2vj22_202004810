package olc2.proyecto1_202004810;

import Environment.Env;
import Environment.Symbol;
import Grammar.gramaticaBaseVisitor;
import Grammar.gramaticaParser;

public class AST extends gramaticaBaseVisitor {

    @Override
    public String visitStart(gramaticaParser.StartContext ctx) {
        String tree = "nodo" + ctx.hashCode() + "[label = \"Start\"];\n";
        tree += "nodo" + ctx.hashCode() + " -> " + visit(ctx.sentencias());
        return tree;
    }

    @Override
    public String visitSentencias(gramaticaParser.SentenciasContext ctx) {
        String tree = "";
        int i = 0, j = 0;
        tree += "nodo" + ctx.hashCode() + i + j + "\n";
        tree += "nodo" + ctx.hashCode() + i + j + "[label = \"Lista sentencias\"];\n";
        tree += "nodo" + ctx.hashCode() + i + j + " -> " + visitSentencia(ctx.sentencia(0));

        i = 1;
        j = 1;
        for (int k = 1; k < ctx.sentencia().size(); k++) {
            tree += "nodo" + ctx.hashCode() + i + j + "\n";
            tree += "nodo" + ctx.hashCode() + i + j + "[label = \"Lista sentencias\"];\n";
            tree += "nodo" + ctx.hashCode() + (i - 1) + (j - 1) + " -> nodo" + ctx.hashCode() + i + j + "\n";

            tree += "nodo" + ctx.hashCode() + i + j + " -> " + visitSentencia(ctx.sentencia(k));
            i++;
            j++;
        }
        return tree;
    }

    @Override
    public String visitSentencia(gramaticaParser.SentenciaContext ctx) {
        String tree = "";
        if (ctx.main() != null) {
            tree = visitMain(ctx.main());
        } else if (ctx.declaracion() != null) {
            tree = visitDeclaracion(ctx.declaracion());
        } else if (ctx.sentencia_if() != null) {
            tree = visitSentencia_if(ctx.sentencia_if());
        } else if (ctx.sentencia_do() != null) {
            tree = visitSentencia_do(ctx.sentencia_do());
        } else if (ctx.sentencia_while() != null) {
            tree = visitSentencia_while(ctx.sentencia_while());
        } else if (ctx.darreglo() != null) {
            tree = visitDarreglo(ctx.darreglo());
        } else if (ctx.dmatriz() != null) {
            tree = visitDmatriz(ctx.dmatriz());
        } else if (ctx.dinamico() != null) {
            tree = (String)visit(ctx.dinamico());
        } else if (ctx.asignacion() != null) {
            tree = (String) visit(ctx.asignacion());
        } else if (ctx.print() != null) {
            tree = visitPrint(ctx.print());
        } else if (ctx.subrutina() != null) {
            tree = visitSubrutina(ctx.subrutina());
        } else if (ctx.callback() != null) {
            tree = visitCallback(ctx.callback());
        } else if (ctx.funcion() != null) {
            tree = visitFuncion(ctx.funcion());
        }

        return tree;
    }

    @Override
    public String visitMain(gramaticaParser.MainContext ctx) {
        String tree = "";
        tree += "nodo" + ctx.hashCode() + "\n";
        tree += "nodo" + ctx.hashCode() + "[label = \"Program\"];\n";
        tree += "nodo" + ctx.hashCode() + " -> " + visit(ctx.sentencias());
        return tree;
    }
    @Override
    public String visitFuncion(gramaticaParser.FuncionContext ctx){
        String tree = "";
        tree += "nodo" + ctx.hashCode() + "\n";
        tree += "nodo" + ctx.hashCode() + "[label = \"Funcion\"];\n";
        tree += "nodo" + ctx.hashCode() + "ID\n";
        tree += "nodo" + ctx.hashCode() + "ID[label = \""+ctx.ID(0).getText()+"\"];\n";
        tree += "nodo" + ctx.hashCode() + " -> nodo"+ctx.hashCode()+"ID;\n";
        tree += "nodo" + ctx.hashCode() + " -> " + visit(ctx.params());
        tree += "nodo" + ctx.hashCode() + " -> " + visit(ctx.sentencias());
        return tree;
    }

    @Override
    public String visitCallfunction(gramaticaParser.CallfunctionContext ctx){
        String tree = "";
        tree += "nodo" + ctx.hashCode() + "\n";
        tree += "nodo" + ctx.hashCode() + "[label = \"CallFuncion\"];\n";
        tree += "nodo" + ctx.hashCode() + "ID\n";
        tree += "nodo" + ctx.hashCode() + "ID[label = \""+ctx.ID().getText()+"\"];\n";
        tree += "nodo" + ctx.hashCode() + " -> nodo"+ctx.hashCode()+"ID;\n";
        tree += "nodo" + ctx.hashCode() + " -> " + visit(ctx.params());
        return tree;
    }
    @Override
    public String visitSubrutina(gramaticaParser.SubrutinaContext ctx){
        String tree = "";
        tree += "nodo" + ctx.hashCode() + "\n";
        tree += "nodo" + ctx.hashCode() + "[label = \"Subrutina\"];\n";
        tree += "nodo" + ctx.hashCode() + "ID\n";
        tree += "nodo" + ctx.hashCode() + "ID[label = \""+ctx.ID(0).getText()+"\"];\n";
        tree += "nodo" + ctx.hashCode() + " -> nodo"+ctx.hashCode()+"ID;\n";
        tree += "nodo" + ctx.hashCode() + " -> " + visit(ctx.params());
        tree += "nodo" + ctx.hashCode() + " -> " + visit(ctx.sentencias());
        return tree;
    }
    @Override
    public String visitCallback(gramaticaParser.CallbackContext ctx){
        String tree = "";
        tree += "nodo" + ctx.hashCode() + "\n";
        tree += "nodo" + ctx.hashCode() + "[label = \"CallSubrutina\"];\n";
        tree += "nodo" + ctx.hashCode() + "ID\n";
        tree += "nodo" + ctx.hashCode() + "ID[label = \""+ctx.ID().getText()+"\"];\n";
        tree += "nodo" + ctx.hashCode() + " -> nodo"+ctx.hashCode()+"ID;\n";
        tree += "nodo" + ctx.hashCode() + " -> " + visit(ctx.params());
        return tree;
    }
    public String visitParams(gramaticaParser.ParamsContext ctx){
        String tree = "";
        tree += "nodo" + ctx.hashCode() + "\n";
        tree += "nodo" + ctx.hashCode() + "[label = \"Parametros\"];\n";

        for (int i = 0; i < ctx.exp().size(); i++) {
            tree += "nodo" + ctx.hashCode() + " -> " + visit(ctx.exp(i));
        }
        return tree;
    }

    public String visitSentencia_while(gramaticaParser.Sentencia_whileContext ctx){
        String tree = "";
        tree += "nodo" + ctx.hashCode() + "\n";
        tree += "nodo" + ctx.hashCode() + "[label = \"DO WHILE\"];\n";
        tree += "nodo" + ctx.hashCode() + " -> " + visit(ctx.exp());
        tree += "nodo" + ctx.hashCode() + " -> " + visit(ctx.sentencias());
        return tree;
    }
    @Override
    public String visitSentencia_do(gramaticaParser.Sentencia_doContext ctx){
        String tree = "";
        tree += "nodo" + ctx.hashCode() + "\n";
        tree += "nodo" + ctx.hashCode() + "[label = \"DO\"];\n";
        tree += "nodo" + ctx.hashCode() + " -> " + visit(ctx.asignacion());
        tree += "nodo" + ctx.hashCode() + " -> " + visit(ctx.sup);
        tree += "nodo" + ctx.hashCode() + " -> " + visit(ctx.asc);
        tree += "nodo" + ctx.hashCode() + " -> " + visit(ctx.sentencias());
        return tree;
    }
    @Override
    public String visitSentencia_if(gramaticaParser.Sentencia_ifContext ctx){
        String tree = "";
        tree += "nodo" + ctx.hashCode() + "\n";
        tree += "nodo" + ctx.hashCode() + "[label = \"IF\"];\n";
        tree += "nodo" + ctx.hashCode() + " -> " + visit(ctx.exp());
        tree += "nodo" + ctx.hashCode() + " -> " + visit(ctx.sentencias());

        if(ctx.salida().size()>0){
            tree += "nodo" + ctx.hashCode() + " -> " + visit(ctx.salida(0));
        }
        int i = 0, j = 0;
        if(ctx.sen_elseif().size()>0){

            tree += "nodo" + ctx.hashCode() + i + j + "\n";
            tree += "nodo" + ctx.hashCode() + i + j + "[label = \"ELSE IF\"];\n";
            tree += "nodo" + ctx.hashCode() + " -> nodo" + ctx.hashCode() + i + j + "\n";
            tree += "nodo" + ctx.hashCode() + i + j + " -> " + visit(ctx.sen_elseif(0));

            i = 1;
            j = 1;
            for (int k = 1; k < ctx.sen_elseif().size(); k++) {
                tree += "nodo" + ctx.hashCode() + i + j + "\n";
                tree += "nodo" + ctx.hashCode() + i + j + "[label = \"ELSE IF\"];\n";
                tree += "nodo" + ctx.hashCode() + (i - 1) + (j - 1) + " -> nodo" + ctx.hashCode() + i + j + "\n";

                tree += "nodo" + ctx.hashCode() + i + j + " -> " + visit(ctx.sen_elseif(k));
                i++;
                j++;
            }
        }

        if(ctx.sen_else().size()>0){
            tree += "nodo" + ctx.hashCode() + " -> " + visit(ctx.sen_else(0));
        }

        return tree;
    }

    @Override
    public String visitSalida(gramaticaParser.SalidaContext ctx){
        String tree = "";
        tree+="nodo"+ctx.hashCode()+"\n";
        tree+="nodo"+ctx.hashCode()+"[label = \""+ctx.getText()+"\"];\n";
        return tree;
    }

    @Override
    public String visitSen_else(gramaticaParser.Sen_elseContext ctx){
        String tree = "";
        tree += "nodo" + ctx.hashCode() + "\n";
        tree += "nodo" + ctx.hashCode() + "[label = \"ELSE\"];\n";
        tree += "nodo" + ctx.hashCode() + " -> " + visit(ctx.sentencias());

        if(ctx.salida().size()>0){
            tree += "nodo" + ctx.hashCode() + " -> " + visit(ctx.salida(0));
        }
        return tree;
    }

    @Override
    public String visitSen_elseif(gramaticaParser.Sen_elseifContext ctx){
        String tree = "";
        tree += "nodo" + ctx.hashCode() + "\n";
        tree += "nodo" + ctx.hashCode() + "[label = \"IF\"];\n";
        tree += "nodo" + ctx.hashCode() + " -> " + visit(ctx.exp());
        tree += "nodo" + ctx.hashCode() + " -> " + visit(ctx.sentencias());

        if(ctx.salida().size()>0){
            tree += "nodo" + ctx.hashCode() + " -> " + visit(ctx.salida(0));
        }
        return tree;
    }
    @Override
    public String visitPrint(gramaticaParser.PrintContext ctx){
        String tree = "";
        tree+="nodo"+ctx.hashCode()+"\n";
        tree+="nodo"+ctx.hashCode()+"[label = \"Print\"];\n";
        for (int i = 0; i < ctx.exp().size(); i++) {
            tree+="nodo"+ctx.hashCode()+" -> "+visit(ctx.exp(i));
        }

        return tree;
    }

    @Override
    public String visitResetD(gramaticaParser.ResetDContext ctx){
        String tree = "";
        tree+="nodo"+ctx.hashCode()+"\n";
        tree+="nodo"+ctx.hashCode()+"[label = \"Deallocate\"];\n";
        tree+="nodo"+ctx.hashCode()+" -> nodo"+ctx.hashCode()+"ID\n";
        tree+="nodo"+ctx.hashCode()+"ID[label = \""+ctx.ID().getText()+"\"];\n";
        return tree;
    }

    @Override
    public String visitTvector(gramaticaParser.TvectorContext ctx){
        String tree = "";
        tree+="nodo"+ctx.hashCode()+"\n";
        tree+="nodo"+ctx.hashCode()+"[label = \"Allocate\"];\n";
        tree+="nodo"+ctx.hashCode()+" -> nodo"+ctx.hashCode()+"ID\n";
        tree+="nodo"+ctx.hashCode()+"ID[label = \""+ctx.ID().getText()+"("+ctx.exp().getText()+")\"];\n";
        return tree;
    }

    @Override
    public String visitTmatriz(gramaticaParser.TmatrizContext ctx){
        String tree = "";
        tree+="nodo"+ctx.hashCode()+"\n";
        tree+="nodo"+ctx.hashCode()+"[label = \"Allocate\"];\n";
        tree+="nodo"+ctx.hashCode()+" -> nodo"+ctx.hashCode()+"ID\n";
        tree+="nodo"+ctx.hashCode()+"ID[label = \""+ctx.ID().getText()+"("+ctx.x.getText()+","+ctx.y.getText()+")\"];\n";
        return tree;
    }

    @Override
    public Object visitDinamicoV(gramaticaParser.DinamicoVContext ctx){
        String tree = "";
        tree+="nodo"+ctx.hashCode()+"\n";
        tree+="nodo"+ctx.hashCode()+"[label = \"Allocatable\"];\n";
        tree+="nodo"+ctx.hashCode()+" -> nodo"+ctx.hashCode()+"ID\n";
        tree+="nodo"+ctx.hashCode()+"ID[label = \""+ctx.ID().getText()+"(:)\"];\n";
        return tree;
    }

    @Override
    public String visitDinamicoM(gramaticaParser.DinamicoMContext ctx){
        String tree = "";
        tree+="nodo"+ctx.hashCode()+"\n";
        tree+="nodo"+ctx.hashCode()+"[label = \"Allocatable\"];\n";
        tree+="nodo"+ctx.hashCode()+" -> nodo"+ctx.hashCode()+"ID\n";
        tree+="nodo"+ctx.hashCode()+"ID[label = \""+ctx.ID().getText()+"(:,:)\"];\n";
        return tree;
    }

    public String visitAvlista(gramaticaParser.AvlistaContext ctx){
        String tree = "";
        tree+="nodo"+ctx.hashCode()+"\n";
        tree+="nodo"+ctx.hashCode()+"[label = \"Asignacion Arreglo\"];\n";
        tree+="nodo"+ctx.hashCode()+" -> nodo"+ctx.hashCode()+"ID\n";
        tree+="nodo"+ctx.hashCode()+"ID[label = \""+ctx.ID().getText()+"\"];\n";
        for (int i = 0; i < ctx.exp().size(); i++) {
            tree+="nodo"+ctx.hashCode()+" -> "+visit(ctx.exp(i));
        }
        return tree;
    }
    @Override
    public String visitAmatriz(gramaticaParser.AmatrizContext ctx){
        String tree = "";
        tree+="nodo"+ctx.hashCode()+"\n";
        tree+="nodo"+ctx.hashCode()+"[label = \"Asignacion Matriz\"];\n";
        tree+="nodo"+ctx.hashCode()+" -> nodo"+ctx.hashCode()+"ID\n";
        tree+="nodo"+ctx.hashCode()+"ID[label = \""+ctx.ID().getText()+"["+ctx.x.getText()+"]["+ctx.y.getText()+"]\"];\n";
        tree+="nodo"+ctx.hashCode()+" -> "+visit(ctx.valor);
        return tree;
    }

    @Override
    public String visitAvector(gramaticaParser.AvectorContext ctx){
        String tree = "";
        tree+="nodo"+ctx.hashCode()+"\n";
        tree+="nodo"+ctx.hashCode()+"[label = \"Asignacion Arreglo\"];\n";
        tree+="nodo"+ctx.hashCode()+" -> nodo"+ctx.hashCode()+"ID\n";
        tree+="nodo"+ctx.hashCode()+"ID[label = \""+ctx.ID().getText()+"["+ctx.index.getText()+"]\"];\n";
        tree+="nodo"+ctx.hashCode()+" -> "+visit(ctx.valor);
        return tree;
    }

    @Override
    public String visitAvariable(gramaticaParser.AvariableContext ctx){
        String tree = "";
        tree+="nodo"+ctx.hashCode()+"\n";
        tree+="nodo"+ctx.hashCode()+"[label = \"Asignacion\"];\n";
        tree+="nodo"+ctx.hashCode()+" -> nodo"+ctx.hashCode()+"ID\n";
        tree+="nodo"+ctx.hashCode()+"ID[label = \""+ctx.ID().getText()+"\"];\n";
        tree+="nodo"+ctx.hashCode()+" -> "+visit(ctx.exp());
        return tree;
    }
    @Override
    public String visitDmatriz(gramaticaParser.DmatrizContext ctx){
        String tree = "";
        tree+="nodo"+ctx.hashCode()+"\n";
        tree+="nodo"+ctx.hashCode()+"[label = \"Declaracion Matriz\"];\n";
        tree+="nodo"+ctx.hashCode()+" -> nodo"+ctx.hashCode()+"ID\n";
        tree+="nodo"+ctx.hashCode()+"ID[label = \""+ctx.ID().getText()+"["+ctx.x.getText()+"]["+ctx.y.getText()+"]\"];\n";
        return tree;
    }

    @Override
    public String visitDarreglo(gramaticaParser.DarregloContext ctx){
        String tree = "";
        tree+="nodo"+ctx.hashCode()+"\n";
        tree+="nodo"+ctx.hashCode()+"[label = \"Declaracion Arreglo\"];\n";
        tree+="nodo"+ctx.hashCode()+" -> nodo"+ctx.hashCode()+"ID\n";
        tree+="nodo"+ctx.hashCode()+"ID[label = \""+ctx.ID().getText()+"["+ctx.exp().getText()+"]\"];\n";
        return tree;
    }
    @Override
    public String visitDeclaracion(gramaticaParser.DeclaracionContext ctx){
        return (String) visit(ctx.ldeclaraciones());
    }
    @Override
    public String visitDindv(gramaticaParser.DindvContext ctx){
        String tree = "";
        tree+="nodo"+ctx.hashCode()+"\n";
        tree+="nodo"+ctx.hashCode()+"[label = \"Declaracion\"];\n";
        tree+="nodo"+ctx.hashCode()+" -> nodo"+ctx.hashCode()+"ID\n";
        tree+="nodo"+ctx.hashCode()+"ID[label = \""+ctx.ID().getText()+"\"];\n";
        tree+="nodo"+ctx.hashCode()+" -> "+visit(ctx.ldeclaraciones());
        return tree;
    }
    @Override
    public String visitDvalor(gramaticaParser.DvalorContext ctx){
        String tree = "";
        tree+="nodo"+ctx.hashCode()+"\n";
        tree+="nodo"+ctx.hashCode()+"[label = \"Declaracion\"];\n";
        tree+="nodo"+ctx.hashCode()+" -> nodo"+ctx.hashCode()+"ID\n";
        tree+="nodo"+ctx.hashCode()+"ID[label = \""+ctx.ID().getText()+"\"];\n";
        tree+="nodo"+ctx.hashCode()+" -> "+visit(ctx.exp());
        tree+="nodo"+ctx.hashCode()+" -> "+visit(ctx.ldeclaraciones());
        return tree;
    }

    @Override
    public Object visitDvalor2(gramaticaParser.Dvalor2Context ctx){
        String tree = "";
        tree+="nodo"+ctx.hashCode()+"\n";
        tree+="nodo"+ctx.hashCode()+"[label = \"Declaracion\"];\n";
        tree+="nodo"+ctx.hashCode()+" -> nodo"+ctx.hashCode()+"ID\n";
        tree+="nodo"+ctx.hashCode()+"ID[label = \""+ctx.ID().getText()+"\"];\n";
        tree+="nodo"+ctx.hashCode()+" -> "+visit(ctx.exp());
        return tree;
    }

    @Override
    public Object visitDindv2(gramaticaParser.Dindv2Context ctx){
        String tree = "";
        tree+="nodo"+ctx.hashCode()+"\n";
        tree+="nodo"+ctx.hashCode()+"[label = \"Declaracion\"];\n";
        tree+="nodo"+ctx.hashCode()+" -> nodo"+ctx.hashCode()+"ID\n";
        tree+="nodo"+ctx.hashCode()+"ID[label = \""+ctx.ID().getText()+"\"];\n";
        return tree;
    }
    @Override
    public String visitOpExpl(gramaticaParser.OpExplContext ctx){
        String tree = "";
        tree+="nodo"+ctx.hashCode()+"\n";
        tree+="nodo"+ctx.hashCode()+"[label = \""+ctx.op.getText()+"\"];\n";

        tree+="nodo"+ctx.hashCode()+" -> "+visit(ctx.left);
        tree+="nodo"+ctx.hashCode()+" -> "+visit(ctx.right);

        return tree;
    }
    @Override
    public String visitOpExpNot(gramaticaParser.OpExpNotContext ctx){
        String tree = "";
        tree+="nodo"+ctx.hashCode()+"\n";
        tree+="nodo"+ctx.hashCode()+"[label = \""+ctx.op.getText()+"\"];\n";
        tree+="nodo"+ctx.hashCode()+" -> "+visit(ctx.right);
        return  tree;
    }
    @Override
    public String visitOpExpr(gramaticaParser.OpExprContext ctx){
        String tree = "";
        tree+="nodo"+ctx.hashCode()+"\n";
        tree+="nodo"+ctx.hashCode()+"[label = \""+ctx.op.getText()+"\"];\n";

        tree+="nodo"+ctx.hashCode()+" -> "+visit(ctx.left);
        tree+="nodo"+ctx.hashCode()+" -> "+visit(ctx.right);

        return tree;
    }
    @Override
    public String visitOpExp(gramaticaParser.OpExpContext ctx){
        String tree = "";
        tree+="nodo"+ctx.hashCode()+"\n";
        tree+="nodo"+ctx.hashCode()+"[label = \""+ctx.op.getText()+"\"];\n";

        tree+="nodo"+ctx.hashCode()+" -> "+visit(ctx.left);
        tree+="nodo"+ctx.hashCode()+" -> "+visit(ctx.right);

        return tree;
    }
    @Override
    public String visitOpExpMenos(gramaticaParser.OpExpMenosContext ctx){
        String tree = "";
        tree+="nodo"+ctx.hashCode()+"\n";
        tree+="nodo"+ctx.hashCode()+"[label = \""+ctx.op.getText()+"\"];\n";
        tree+="nodo"+ctx.hashCode()+" -> "+visit(ctx.right);
        return  tree;
    }
    public String visitParenExp(gramaticaParser.ParenExpContext ctx){
        return (String) visit(ctx.exp());
    }
    @Override
    public String visitValorArr(gramaticaParser.ValorArrContext ctx){
        String tree = "";
        tree+="nodo"+ctx.hashCode()+"\n";
        tree+="nodo"+ctx.hashCode()+"[label = \"size\"];\n";
        tree+="nodo"+ctx.hashCode()+"ID\n";
        tree+="nodo"+ctx.hashCode()+"ID[label = \""+ctx.ID().getText()+"\"];\n";
        tree+="nodo"+ctx.hashCode()+" -> nodo"+ctx.hashCode()+"ID\n";
        return  tree;
    }
    @Override
    public String visitLiteralArr(gramaticaParser.LiteralArrContext ctx){
        String tree = "";
        tree+="nodo"+ctx.hashCode()+"\n";
        tree+="nodo"+ctx.hashCode()+"[label = \""+ctx.ID().getText()+"["+ctx.exp().getText()+"]\"];\n";
        return tree;

    }
    @Override
    public String visitLiteralMtx(gramaticaParser.LiteralMtxContext ctx){
        String tree = "";
        tree+="nodo"+ctx.hashCode()+"\n";
        tree+="nodo"+ctx.hashCode()+"[label = \""+ctx.ID().getText()+"["+ctx.x.getText()+"]["+ctx.y.getText()+"]\"];\n";
        return tree;

    }

    @Override
    public String visitLiteralID(gramaticaParser.LiteralIDContext ctx){
        String tree = "";
        tree+="nodo"+ctx.hashCode()+"\n";
        tree+="nodo"+ctx.hashCode()+"[label = \""+ctx.ID().getText()+"\"];\n";
        return tree;
    }

    @Override
    public String visitLiteralINT(gramaticaParser.LiteralINTContext ctx){
        String tree = "";
        tree+="nodo"+ctx.hashCode()+"\n";
        tree+="nodo"+ctx.hashCode()+"[label = \""+ctx.getText()+"\"];\n";
        return tree;
    }
    @Override
    public String visitLiteralCHR(gramaticaParser.LiteralCHRContext ctx){
        String tree = "";
        tree+="nodo"+ctx.hashCode()+"\n";
        tree+="nodo"+ctx.hashCode()+"[label = \""+ctx.getText().charAt(1)+"\"];\n";
        return tree;
    }

    @Override
    public String visitLiteralBOL(gramaticaParser.LiteralBOLContext ctx){
        String tree = "";
        tree+="nodo"+ctx.hashCode()+"\n";
        tree+="nodo"+ctx.hashCode()+"[label = \""+ctx.getText()+"\"];\n";
        return tree;
    }

    @Override
    public String visitLiteralDEC(gramaticaParser.LiteralDECContext ctx){
        String tree = "";
        tree+="nodo"+ctx.hashCode()+"\n";
        tree+="nodo"+ctx.hashCode()+"[label = \""+ctx.getText()+"\"];\n";
        return tree;
    }

    @Override
    public String visitLiteralSTR(gramaticaParser.LiteralSTRContext ctx){
        String tree = "";
        tree+="nodo"+ctx.hashCode()+"\n";
        tree+="nodo"+ctx.hashCode()+"[label = \""+ctx.getText().substring(1,ctx.getText().length()-1)+"\"];\n";
        return tree;
    }

    @Override
    public String visitTipo(gramaticaParser.TipoContext ctx){
        String tree = "";
        tree+="nodo"+ctx.hashCode()+"\n";
        tree+="nodo"+ctx.hashCode()+"[label = \""+ctx.getText()+"\"];\n";
        return tree;
    }
}
