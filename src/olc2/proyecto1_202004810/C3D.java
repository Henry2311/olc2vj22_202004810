package olc2.proyecto1_202004810;

import Environment.*;
import Grammar.gramaticaBaseVisitor;
import Grammar.gramaticaParser;
import org.antlr.v4.runtime.tree.ParseTree;

import java.util.ArrayList;
import java.util.Stack;

public class C3D extends gramaticaBaseVisitor {
    Env preview;
    Stack<Env> pilaENV;
    threeDirectionCode c3d = new threeDirectionCode();
    boolean express = false;
    String aux_tipos = "",retorno_P = "";
    public C3D(Env preview, Stack<Env> pilaENV) {
        this.preview = preview;
        this.pilaENV = pilaENV;
        this.pilaENV.push(preview);
    }

    @Override
    public Object visitStart(gramaticaParser.StartContext ctx) {
        return visit(ctx.sentencias());
    }

    @Override
    public Object visitSentencias(gramaticaParser.SentenciasContext ctx){
        for (gramaticaParser.SentenciaContext ictx : ctx.sentencia()){
            Object salida = visit(ictx);
            if(salida.equals("exit") | salida.equals("cycle")){
                return salida;
            }
        }
        return true;
    }

    @Override
    public Object visitSentencia(gramaticaParser.SentenciaContext ctx) {
        if (ctx.main() != null) {
           visitMain(ctx.main());
        } else if (ctx.declaracion() != null) {
            visitDeclaracion(ctx.declaracion());
        }else if (ctx.sentencia_if() != null) {
            visitSentencia_if(ctx.sentencia_if());
        } else if (ctx.sentencia_do() != null) {
            visitSentencia_do(ctx.sentencia_do());
        } else if (ctx.sentencia_while() != null) {
            visitSentencia_while(ctx.sentencia_while());
        } else if (ctx.darreglo() != null) {
            visitDarreglo(ctx.darreglo());
        } else if (ctx.dmatriz() != null) {
            visitDmatriz(ctx.dmatriz());
        } else if (ctx.dinamico() != null) {
           visit(ctx.dinamico());
        } else if (ctx.asignacion() != null) {
            visit(ctx.asignacion());
        } else if (ctx.print() != null) {
            visitPrint(ctx.print());
        } else if (ctx.subrutina() != null) {
            visitSubrutina(ctx.subrutina());
        } else if (ctx.callback() != null) {
            visitCallback(ctx.callback());
        } else if (ctx.funcion() != null) {
            visitFuncion(ctx.funcion());
        }

        return true;
    }

    @Override
    public Object visitMain(gramaticaParser.MainContext ctx){
        Env e = pilaENV.peek().next;
        pilaENV.push(e);
        c3d.code.add("int main(){");
        visit(ctx.sentencias());
        c3d.code.add("return 0;");
        c3d.code.add("}");
        pilaENV.pop();
        return true;
    }
    @Override
    public Object visitSubrutina(gramaticaParser.SubrutinaContext ctx){
        Env e = pilaENV.peek();
        if(e.exist(ctx.id1.getText())){
            Symbol s = e.search(ctx.id1.getText());
            Symbol_routine sub = (Symbol_routine) s.getValue();
            c3d.code.add("void "+sub.id+"(){\n");
            pilaENV.push(sub.ent);
            visitSentencias((gramaticaParser.SentenciasContext) sub.sentencias);
            pilaENV.pop();
            c3d.code.add("\treturn;\n}\n");
        }
        return true;
    }
    @Override
    public Object visitCallback(gramaticaParser.CallbackContext ctx){
        Env e = pilaENV.peek();
        Symbol routine = e.search(ctx.ID().getText());
        if(routine!=null){
            Symbol_routine sub = (Symbol_routine)routine.getValue();
            if(sub.params.size()==ctx.params().exp().size()){
                for (int i = 0; i < ctx.params().exp().size(); i++) {
                    Symbol v = sub.params.get(i);
                    c3d.code.add("\tT"+c3d.generateTemporal()+" = "+v.getPosition()+";");
                    c3d.code.add("\tP = T"+c3d.lastTemporal()+";");
                    String value = (String) visit(ctx.params().exp(i));
                    if(express){
                        c3d.code.add("\tSTACK[(int)P] = T" + (c3d.lastTemporal()-1) + ";");
                    }else{c3d.code.add("\tSTACK[(int)P] = " + value + ";");}
                }
                c3d.code.add(ctx.ID().getText()+"();");
            }
        }

        return true;
    }
    @Override
    public Object visitFuncion(gramaticaParser.FuncionContext ctx){
        Env e = pilaENV.peek();
        if(e.exist(ctx.id1.getText())){
            Symbol s = e.search(ctx.id1.getText());
            Symbol_function sub = (Symbol_function) s.getValue();
            c3d.code.add("void "+sub.id+"(){\n");
            pilaENV.push(sub.ent);
            visitSentencias((gramaticaParser.SentenciasContext) sub.sentencias);
            retorno_P = (String) visit(ctx.res);
            pilaENV.pop();
            c3d.code.add("\treturn;\n}\n");
        }
        return true;
    }
    @Override
    public Object visitCallfunction(gramaticaParser.CallfunctionContext ctx){
        Env e = pilaENV.peek();
        Symbol routine = e.search(ctx.ID().getText());
        String T = "";
        if(routine!=null){
            Symbol_function sub = (Symbol_function) routine.getValue();
            if(sub.params.size()==ctx.params().exp().size()){
                for (int i = 0; i < ctx.params().exp().size(); i++) {
                    Symbol v = sub.params.get(i);
                    c3d.code.add("\tT"+c3d.generateTemporal()+" = "+v.getPosition()+";");
                    c3d.code.add("\tP = T"+c3d.lastTemporal()+";");
                    String value = (String) visit(ctx.params().exp(i));
                    if(express){
                        c3d.code.add("\tSTACK[(int)P] = T" + (c3d.lastTemporal()-1) + ";");
                    }else{c3d.code.add("\tSTACK[(int)P] = " + value + ";");}
                }
                c3d.code.add(ctx.ID().getText()+"();");
               T = retorno_P;
            }
        }
        return T;
    }
    @Override
    public Object visitSentencia_while(gramaticaParser.Sentencia_whileContext ctx){
        String label_ciclo_inicio = "L"+c3d.generateLabel();
        String casoV = "L"+c3d.generateLabel();
        String casoF = "L"+c3d.generateLabel();

        c3d.code.add("\t"+label_ciclo_inicio+":");
        String T = (String) visit(ctx.exp());
        c3d.code.add("\tif("+T+") goto "+casoV+";\n"+
                "\tgoto "+casoF+";\n");

        c3d.code.add("\t"+casoV+": ");
        Env e = pilaENV.peek().next;
        pilaENV.push(e);
        visit(ctx.sentencias());
        pilaENV.pop();
        c3d.code.add("goto "+label_ciclo_inicio+";");
        c3d.code.add("\t"+casoF+": ");
        return true;
    }
    @Override
    public Object visitSentencia_do(gramaticaParser.Sentencia_doContext ctx){
        visit(ctx.asignacion());
        String val = "T"+c3d.generateTemporal();
        c3d.code.add("\t"+val+" = STACK[(int)P];"); //Este seria el valor inicial de i

        String sup = "T"+c3d.generateTemporal();
        String value = (String) visit(ctx.sup);
        if(express){
            c3d.code.add("\t"+sup +" = "+ (c3d.lastTemporal()-1) + ";");
        }else{c3d.code.add("\t"+sup +" = "+ value + ";");}

        String inc = "T"+c3d.generateTemporal();
        value = (String) visit(ctx.asc);
        if(express){
            c3d.code.add("\t"+inc +" = "+ (c3d.lastTemporal()-1) + ";");
        }else{c3d.code.add("\t"+inc +" = "+ value + ";");}

        String label_ciclo_inicio = "L"+c3d.generateLabel();
        c3d.code.add("\t"+label_ciclo_inicio+": ");
        c3d.code.add("\tif("+val+"<="+sup+") goto L"+c3d.generateLabel()+";");
        String label_ciclo_fin = "L"+c3d.generateLabel();
        c3d.code.add("\tgoto "+label_ciclo_fin+";");
        c3d.code.add("\tL"+(c3d.lastLabel()-1)+": ");

        Env e = pilaENV.peek().next;
        pilaENV.push(e);
        visit(ctx.sentencias());
        pilaENV.pop();

        c3d.code.add("\t"+val+" = "+val+" + "+inc+";");
        generateAsignation(ctx.asignacion().children.get(0).getText(),val);
        c3d.code.add("\tgoto " +label_ciclo_inicio+ ";");
        c3d.code.add("\t"+label_ciclo_fin+":");

        return true;
    }
    private void generateAsignation(String id, String value){
        Env e = pilaENV.peek();
        if(e.exist(id)){
            Symbol s = e.search(id);
            c3d.code.add("\tT"+c3d.generateTemporal()+" = "+s.getPosition()+";");
            c3d.code.add("\tP = T"+ c3d.lastTemporal()+";");
            c3d.code.add("\tSTACK[(int)P] = " + value + ";");
        }
    }
    @Override
    public Object visitSentencia_if(gramaticaParser.Sentencia_ifContext ctx){
        String T = (String) visit(ctx.exp());
        String casoV = "L"+c3d.generateLabel();
        String casoF = "L"+c3d.generateLabel();
        c3d.code.add("\tif("+T+") goto "+casoV+";\n"+
                "\tgoto "+casoF+";\n");
        c3d.code.add("\t"+casoV+": ");

        Env e = pilaENV.peek();
        pilaENV.push(e);
        visit(ctx.sentencias());
        pilaENV.pop();
        String lastLabelIf = "L"+c3d.generateLabel();

        if(ctx.sen_else().size()==0 && ctx.sen_elseif().size()==0){
            c3d.code.add("\tgoto "+lastLabelIf+";\n");
            c3d.code.add("\t"+casoF+": ");
        }
        ArrayList<String> lastLabel = new ArrayList<>();
        if(ctx.sen_elseif().size()>0){
            c3d.code.add("\tgoto "+lastLabelIf+";\n");
            c3d.code.add("\tL"+(c3d.lastLabel()-1)+":");
            for (int i = 0; i < ctx.sen_elseif().size(); i++) {
                lastLabel.add((String)visit(ctx.sen_elseif(i)));
                if(ctx.sen_else().size()==0){
                    c3d.code.add("\tL"+(c3d.lastLabel()-1)+": ");
                }else if(ctx.sen_else().size()>0 && (i+1)<ctx.sen_elseif().size()){
                    c3d.code.add("\tL"+(c3d.lastLabel()-1)+": ");
                }
            }
        }
        if(ctx.sen_else().size()>0){
            c3d.code.add("\tgoto "+lastLabelIf+";\n");
            c3d.code.add("\tL"+(c3d.lastLabel()-1)+":");
            for (int i = 0; i < ctx.sen_else().size(); i++) {
                lastLabel.add((String) visit(ctx.sen_else(i)));
            }
        }
        for (String label: lastLabel) {
            c3d.code.add(label+":");
        }
        c3d.code.add(lastLabelIf+":");

        return true;
    }
    @Override
    public Object visitSalida(gramaticaParser.SalidaContext ctx){
        return ctx.getText();
    }
    @Override
    public Object visitSen_elseif(gramaticaParser.Sen_elseifContext ctx){
        String T = (String) visit(ctx.exp());
        c3d.code.add("\tif("+T+") goto L"+c3d.generateLabel()+";\n"+
                "\tgoto L"+c3d.generateLabel()+";\n");
        c3d.code.add("\tL"+(c3d.lastLabel()-1)+": ");

        Env e = pilaENV.peek();
        pilaENV.push(e);
        visit(ctx.sentencias());
        pilaENV.pop();

        String lastLabelIf = "L"+c3d.generateLabel();
        c3d.code.add("\tgoto "+lastLabelIf+";\n");
        return lastLabelIf;
    }
    @Override
    public Object visitSen_else(gramaticaParser.Sen_elseContext ctx){
        Env e = pilaENV.peek();
        pilaENV.push(e);
        visit(ctx.sentencias());
        pilaENV.pop();
        String lastLabelIf = "L"+c3d.generateLabel();
        c3d.code.add("\tgoto "+lastLabelIf+";\n");
        return lastLabelIf;
    }
    @Override
    public Object visitPrint(gramaticaParser.PrintContext ctx){
        for (int i = 0; i < ctx.exp().size(); i++) {
            visit(ctx.exp(i));
            Object n_temporal;
            if(express){
                n_temporal ="T"+c3d.lastTemporal();
            }else{
                n_temporal = visit(ctx.exp(i));
            }
            switch (aux_tipos) {
                case "integer", "logical" -> c3d.code.add("\tprintf(\"%d\\n\",(int) " + n_temporal + ");");
                case "real" -> c3d.code.add("\tprintf(\"%f\\n\"," + n_temporal + ");");
                case "complex" -> c3d.code.add("\tprintf(\"9.192517926E-43,0.0\");");
                case "character" -> c3d.code.add("\tprintf(\"%c\\n\",(int) " + n_temporal + ");");
                case "string" -> c3d.code.add("\tP = "+n_temporal+";\n\tprint_string();");
            }
            aux_tipos = "";
            express = false;
        }
        return true;
    }
    @Override
    public Object visitAvariable(gramaticaParser.AvariableContext ctx){
        Env e = pilaENV.peek();
        if(e.exist(ctx.ID().getText())){
            Symbol s = e.search(ctx.ID().getText());
            String value = (String) visit(ctx.exp());
            c3d.code.add("\tT"+c3d.generateTemporal()+" = "+s.getPosition()+";");
            c3d.code.add("\tP = T"+ c3d.lastTemporal()+";");
            if(express){
                c3d.code.add("\tSTACK[(int)P] = T" + (c3d.lastTemporal()-1) + ";");
            }else{c3d.code.add("\tSTACK[(int)P] = " + value + ";");}
            express = false;
        }
        return true;
    }
    @Override
    public Object visitDeclaracion(gramaticaParser.DeclaracionContext ctx){
        return visit(ctx.ldeclaraciones());
    }
    @Override
    public Object visitDindv(gramaticaParser.DindvContext ctx){
        Env e = pilaENV.peek();
        if(e.exist(ctx.ID().getText())){
            Symbol s = e.search(ctx.ID().getText());
            c3d.code.add("\tT"+c3d.generateTemporal()+" = "+s.getPosition()+";");
            c3d.code.add("\tP = T"+ c3d.lastTemporal()+";");
            switch (s.getType()) {
                case "integer" -> c3d.code.add("\tSTACK[(int)P] = 0;");
                case "real" -> c3d.code.add("\tSTACK[(int)P] = 0.0;");
                case "logical" -> c3d.code.add("\tSTACK[(int)P] = F;");
                case "complex" -> c3d.code.add("\tSTACK[(int)P] = c;");
                case "character" -> c3d.code.add("\tSTACK[(int)P] = 1;");
            }

        }

        return visit(ctx.ldeclaraciones());
    }
    @Override
    public Object visitDvalor(gramaticaParser.DvalorContext ctx){
        Env e = pilaENV.peek();
        if(e.exist(ctx.ID().getText())){
            Symbol s = e.search(ctx.ID().getText());
            String value = (String) visit(ctx.exp());
            c3d.code.add("\tT"+c3d.generateTemporal()+" = "+s.getPosition()+";");
            c3d.code.add("\tP = T"+ c3d.lastTemporal()+";");
            if(express){
                c3d.code.add("\tSTACK[(int)P] = T" + (c3d.lastTemporal()-1) + ";");
            }else{c3d.code.add("\tSTACK[(int)P] = " + value + ";");}
            express = false;
        }
        return visit(ctx.ldeclaraciones());
    }
    @Override
    public Object visitDvalor2(gramaticaParser.Dvalor2Context ctx){
        Env e = pilaENV.peek();
        if(e.exist(ctx.ID().getText())){
            Symbol s = e.search(ctx.ID().getText());
            String value = (String) visit(ctx.exp());
            c3d.code.add("\tT"+c3d.generateTemporal()+" = "+s.getPosition()+";");
            c3d.code.add("\tP = T"+ c3d.lastTemporal()+";");
            if(express){
                c3d.code.add("\tSTACK[(int)P] = T" + (c3d.lastTemporal()-1) + ";");
            }else{
                c3d.code.add("\tSTACK[(int)P] = " + value + ";");}
            express = false;
        }
        return true;
    }
    @Override
    public Object visitDindv2(gramaticaParser.Dindv2Context ctx){
        Env e = pilaENV.peek();
        if(e.exist(ctx.ID().getText())){
            Symbol s = e.search(ctx.ID().getText());
            c3d.code.add("\tT"+c3d.generateTemporal()+" = "+s.getPosition()+";");
            c3d.code.add("\tP = T"+ c3d.lastTemporal()+";");
            switch (s.getType()) {
                case "integer" -> c3d.code.add("\tSTACK[(int)P] = 0;");
                case "real" -> c3d.code.add("\tSTACK[(int)P] = 0.0;");
                case "logical" -> c3d.code.add("\tSTACK[(int)P] = 0;");
                case "complex" -> c3d.code.add("\tSTACK[(int)P] = 1;");
                case "character" -> c3d.code.add("\tSTACK[(int)P] = 2;");
            }
        }
        return true;
    }
    @Override
    public String visitOpExpl(gramaticaParser.OpExplContext ctx){
        String izq = (String) visit(ctx.left);
        String der = (String) visit(ctx.right);
        String op = ctx.op.getText();
        String temp = "";
        switch (op){
            case ".and." -> temp = c3d.getAND(izq,der);
            case ".or." -> temp = c3d.getOR(izq,der);
        }
        return temp;
    }
    @Override
    public String visitOpExpNot(gramaticaParser.OpExpNotContext ctx){
        String exp = (String) visit(ctx.right);
        return c3d.getNOT(exp);
    }
    @Override
    public String visitOpExpr(gramaticaParser.OpExprContext ctx){
        String temporal = "T"+c3d.generateTemporal();
        String izq = (String) visit(ctx.left);
        String der = (String) visit(ctx.right);
        String op = ctx.op.getText();
        if(!izq.equals("") && !der.equals("")){
            switch (op) {
                case "==", ".eq." -> c3d.code.add("\t" + temporal + " = " + izq + "==" + der + ";");
                case "/=", ".ne." -> c3d.code.add("\t" + temporal + " = " + izq + "!=" + der + ";");
                case ">", ".gt." -> c3d.code.add("\t" + temporal + " = " + izq + ">" + der + ";");
                case "<", ".lt." -> c3d.code.add("\t" + temporal + " = " + izq + "<" + der + ";");
                case ">=", ".ge." -> c3d.code.add("\t" + temporal + " = " + izq + ">=" + der + ";");
                case "<=", ".le." -> c3d.code.add("\t" + temporal + " = " + izq + "<=" + der + ";");
            }
        }
        return temporal;
    }
    @Override
    public String visitOpExp(gramaticaParser.OpExpContext ctx){
        String code3d = "";
        express = true;
        String izq = (String) visit(ctx.left);
        String der = (String) visit(ctx.right);
        String op = ctx.op.getText();
        if(op.equals("**")){
            if(!izq.equals("") && !der.equals("")){
                c3d.code.add("\tT"+c3d.generateTemporal()+" = pow("+izq+","+der+");");
            }else if(izq.equals("") && !der.equals("")){
                c3d.code.add("\tT"+c3d.generateTemporal()+" = pow(T"+(c3d.lastTemporal()-1)+","+der+");");
            }else if(!izq.equals("") && der.equals("")){
                c3d.code.add("\tT"+c3d.generateTemporal()+" = pow("+izq+","+"T"+(c3d.lastTemporal()-1)+");");
            }else {
                c3d.code.add("\tT"+c3d.generateTemporal()+" = pow(T"+(c3d.lastTemporal()-2)+","+"T"+(c3d.lastTemporal()-1)+");");
            }
        }else{
            if(!izq.equals("") && !der.equals("")){
                c3d.code.add("\tT"+c3d.generateTemporal()+" = "+izq+op+der+";");
            }else if(izq.equals("") && !der.equals("")){
                c3d.code.add("\tT"+c3d.generateTemporal()+" = T"+(c3d.lastTemporal()-1)+op+der+";");
            }else if(!izq.equals("") && der.equals("")){
                c3d.code.add("\tT"+c3d.generateTemporal()+" = "+izq+op+"T"+(c3d.lastTemporal()-1)+";");
            }else {
                c3d.code.add("\tT"+c3d.generateTemporal()+" = T"+(c3d.lastTemporal()-2)+op+"T"+(c3d.lastTemporal()-1)+";");
            }
        }
        return code3d;
    }
    @Override
    public String visitOpExpMenos(gramaticaParser.OpExpMenosContext ctx){
        String der = (String) visit(ctx.right);
        String op = ctx.op.getText();
        c3d.code.add("\tT"+c3d.generateTemporal()+" = 0"+op+der+";");
        return  "";
    }
    public String visitParenExp(gramaticaParser.ParenExpContext ctx){
        return (String) visit(ctx.exp());
    }
    @Override
    public Object visitLiteralID(gramaticaParser.LiteralIDContext ctx){
        Env e = pilaENV.peek();
        if(e.exist(ctx.ID().getText())){
            Symbol s = e.search(ctx.ID().getText());
            aux_tipos = s.getType();
            c3d.code.add("\tT"+c3d.generateTemporal()+" = "+s.getPosition()+";");
            c3d.code.add("\tP = T"+ c3d.lastTemporal()+";");
            c3d.code.add("\tT"+c3d.generateTemporal()+"= STACK[(int)P];");

        }
        return "T"+c3d.lastTemporal();
    }
    @Override
    public String visitLiteralINT(gramaticaParser.LiteralINTContext ctx){
        aux_tipos = "integer";
        return ctx.getText();
    }
    @Override
    public String visitLiteralCHR(gramaticaParser.LiteralCHRContext ctx){
        aux_tipos = "character";
        int character = ctx.getText().charAt(1);
        return String.valueOf(character);
    }
    @Override
    public String visitLiteralBOL(gramaticaParser.LiteralBOLContext ctx){
        aux_tipos = "logical";
        if(ctx.getText().equals(".true.")){
            return "1";
        }
        return "0";
    }
    @Override
    public String visitLiteralDEC(gramaticaParser.LiteralDECContext ctx){
        aux_tipos = "real";
        return ctx.getText();
    }
    @Override
    public String visitLiteralSTR(gramaticaParser.LiteralSTRContext ctx){
        aux_tipos = "string";
        int aux = c3d.generateTemporal();
        String content = ctx.getText().substring(1,ctx.getText().length()-1);
        c3d.code.add("\tT"+aux+" = H;");
        for (char c : content.toCharArray()) {
            c3d.code.add("\tHEAP[(int)H] = " + (int)c + ";");
            c3d.code.add("\tH = H + 1;");
        }
        c3d.code.add("\tHEAP[(int)H] = -1;");
        c3d.code.add("\tH = H + 1;");
        return "T"+aux;
    }
    @Override
    public String visitTipo(gramaticaParser.TipoContext ctx){
        return ctx.getText();
    }
}
