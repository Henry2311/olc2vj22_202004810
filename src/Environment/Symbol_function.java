
package Environment;

import Grammar.gramaticaParser;
import java.util.ArrayList;


public class Symbol_function {
    public String id;
    public ArrayList<Symbol> params;
    public Object sentencias;
    public gramaticaParser.DecparamsContext decParam;
    public gramaticaParser.ExpContext res;
    public Env ent = new Env(null);
    public Symbol_function(String id, ArrayList<Symbol> params, Object sentencias, gramaticaParser.DecparamsContext decParam,gramaticaParser.ExpContext res) {
        this.id = id;
        this.params = params;
        this.sentencias = sentencias;
        this.decParam = decParam;
        this.res = res;
    }
    
}
