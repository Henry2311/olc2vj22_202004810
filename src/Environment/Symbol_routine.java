
package Environment;

import Grammar.gramaticaParser;
import java.util.ArrayList;

public class Symbol_routine {
    public String id;
    public ArrayList<Symbol> params;
    public Object sentencias;
    public gramaticaParser.DecparamsContext decParam;
    public Env ent = new Env(null);
    public Symbol_routine(String id, ArrayList<Symbol> params, Object sentencias, gramaticaParser.DecparamsContext decParam) {
        this.id = id;
        this.params = params;
        this.sentencias = sentencias;
        this.decParam = decParam;
    }
    
    
}
