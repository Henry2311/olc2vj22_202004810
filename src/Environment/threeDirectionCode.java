package Environment;

import java.util.ArrayList;

public class threeDirectionCode {
    public ArrayList<String> code;
    private int temporal;
    private int label;

    public threeDirectionCode() {
        this.code = new ArrayList<>();
        this.temporal = -1;
        this.label = -1;
    }

    public int generateTemporal(){
        this.temporal++;
        return this.temporal;
    }

    public int lastTemporal(){
        return this.temporal;
    }

    public int generateLabel(){
        this.label++;
        return this.label;
    }
    public int lastLabel(){
        return this.label;
    }
    public String getAND(String left,String right){
        String temp = "T"+this.generateTemporal();
        this.code.add("\tif("+left+") goto L"+this.generateLabel()+";\n"+
                "\tgoto L"+this.generateLabel()+";\n"+
                "\tL"+(this.lastLabel()-1)+": if("+right+") goto L"+(this.lastLabel()+1)+";\n"+
                "\tgoto L"+(this.lastLabel()+2)+";\n"+
                "\tL"+this.generateLabel()+": "+temp+" = 1;\n"+
                "\tgoto L"+(this.lastLabel()+2)+";\n"+
                "\tL"+(this.lastLabel()-1)+": L"+this.generateLabel()+": "+temp+" = 0;\n"+
                "\tL"+this.generateLabel()+": \n");
        return temp;
    }
    public String getOR(String left, String right){
        String temp = "T"+this.generateTemporal();
        this.code.add("\tif (" + left + " ) goto L" + this.generateLabel() + ";\n" +
                "\tgoto L" + this.generateLabel() + ";\n" +
                "\tL" + (this.lastLabel()) + ": if (" + right + ") goto L" + (this.lastLabel() - 1) + ";\n" +
                "\tgoto L" + (this.lastLabel() + 1) + ";\n" +
                "\tL"+ (this.lastLabel() - 1) + ": " + temp + " = 1;\n" +
                "\tgoto L" + (this.lastLabel() + 2) + ";\n" +
                "\tL"+this.generateLabel()+ ":"+ temp+"= 0;\n"+
                "\tgoto L"+this.generateLabel()+";\n"+
                "\tL"+this.lastLabel()+":\n");
        return temp;
    }
    public String getNOT(String exp){
        String temp = "T"+this.generateTemporal();
        this.code.add("\tif (" + exp + " ) goto L" + this.generateLabel() + ";\n" +
                "\tgoto L" + this.generateLabel() + ";\n" +
                "\tL" + (this.lastLabel()-1) + ": " +temp+ "= 0;\n" +
                "\tgoto L" + (this.lastLabel() + 1) + ";\n" +
                "\tL"+ (this.lastLabel()) + ": " + temp + " = 1;\n" +
                "\tL"+this.generateLabel()+ ":\n");
        return temp;
    }
    private String getPrintStr(){
        int tempStart = this.generateTemporal();
        int labelStart = this.generateLabel();
        return "void print_string(){\n" +
                "\tT"+tempStart + " = P;\n" +
                "\tL"+labelStart + ":\n" +
                "\tT"+this.generateTemporal() + " = HEAP[(int) T" + tempStart + "];\n" +
                "\tif (T" + this.lastTemporal() + " != -1) goto L" + (this.label + 1) + ";\n" +
                "\tgoto L" + (this.label + 2) + ";\n" +
                "\tL"+this.generateLabel() + ":\n" +
                "\tprintf(\"%c\", (char) T" + this.lastTemporal() + ");\n" +
                "\tT"+tempStart + "= T" + tempStart + " + 1;\n" +
                "\tgoto L" + labelStart + ";\n" +
                "\tL"+this.generateLabel() + ":\n" +
                "\tprintf(\"%c\\n\", (char)32);\n" +
                "\treturn;\n" +
                "}\n\n";
    }
    public String getHeader(){
        String temps = "";
        String fun = this.getPrintStr();
        for (int i = 0; i <= this.temporal; i++)
            temps += "T" + i + (i < this.temporal ? "," : ";\n");

        return  "#include <stdio.h>\n" +
                "#include <math.h>\n"+
                "double STACK[30101999];\n" +
                "double HEAP[30101999];\n" +
                "double P;\n" +
                "double H;\n" +
                (this.temporal!=-1?"double ":"" )+temps+ "\n"+
                fun;
    }
}
