
package Environment;

public class Symbol {
    private final String id;
    private final String type;
    private Object value;
    private Object[] value_arr;
    private Object[][] value_mtx;
    private int position;
    private final int line;
    private final int column;
    public Symbol(String id,String type, Object value, int position, int line, int column) {
        this.id = id;
        this.type = type;
        this.value = value;
        this.position = position;
        this.line = line;
        this.column = column;
    }

    public Symbol(String id,String type, Object [] value, int line, int column) {
        this.id = id;
        this.type = type;
        this.value_arr = value;
        this.line = line;
        this.column = column;
    }
    
    public Symbol(String id, String type, Object [][] value, int line, int column) {
        this.id = id;
        this.type = type;
        this.value_mtx = value;
        this.line = line;
        this.column = column;
    }

    public Object[][] getValue_mtx() {
        return value_mtx;
    }

    public Object[] getValue_arr() {
        return value_arr;
    }

    public String getType() {
        return type;
    }

    public Object getValue() {
        return value;
    }

    public int getPosition() {
        return position;
    }
    public int getLine() {
        return line;
    }
    public int getColumn() {
        return column;
    }

    public String getId(){
        return id;
    }

    public String html(int i){
        String rows="";
        rows+="<tr>";
        rows+="<td  style=\"color: white; font-size: 1.2rem;\">"+i+"</td>\n";
        rows+="<td  style=\"color: white; font-size: 1.2rem;\">"+this.id+"</td>\n";
        rows+="<td  style=\"color: white; font-size: 1.2rem;\">"+this.type+"</td>\n";

        if(this.value != null)rows+="<td  style=\"color: white; font-size: 1.2rem;\">"+this.value+"</td>\n";
        else if (this.value_arr != null) {
            StringBuilder valor = new StringBuilder("[");
            for (Object o : value_arr) {
                valor.append(o.toString()).append(",");
            }
            valor = new StringBuilder(valor.substring(0, valor.length() - 1));
            valor.append("]");
            rows+="<td  style=\"color: white; font-size: 1.2rem;\">"+valor+"</td>\n";
        }else if(this.value_mtx != null){
            StringBuilder valor = new StringBuilder("[");
            for (Object[] valueMtx : value_mtx) {
                valor.append("[");
                for (Object mtx : valueMtx) {
                    valor.append(mtx.toString()).append(",");
                }
                valor = new StringBuilder(valor.substring(0, valor.length() - 1));
                valor.append("],");
            }
            valor = new StringBuilder(valor.substring(0, valor.length() - 1));
            valor.append("]");
            rows+="<td  style=\"color: white; font-size: 1.2rem;\">"+valor+"</td>\n";
        }
        rows+="<td  style=\"color: white; font-size: 1.2rem;\">"+this.line+"</td>\n";
        rows+="<td  style=\"color: white; font-size: 1.2rem;\">"+this.column+"</td>\n";
        rows+="</tr>";
        return rows;
    }
}
