package Environment;

public class Symbol_error {
    private String type;
    private String description;
    private int row;
    private int column;

    public Symbol_error(String type, String description, int row, int column) {
        this.type = type;
        this.description = description;
        this.row = row;
        this.column = column;
    }

    public String html(int i){
        String rows="";
        rows+="<tr>";
        rows+="<td  style=\"color: red; font-size: 1.2rem;\">"+i+"</td>\n";
        rows+="<td  style=\"color: red; font-size: 1.2rem;\">"+this.type+"</td>\n";
        rows+="<td  style=\"color: red; font-size: 1.2rem;\">"+this.description+"</td>\n";
        rows+="<td  style=\"color: red; font-size: 1.2rem;\">"+this.row+"</td>\n";
        rows+="<td  style=\"color: red; font-size: 1.2rem;\">"+this.column+"</td>\n";
        rows+="</tr>";
        return rows;
    }

}
