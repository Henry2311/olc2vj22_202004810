
package Environment;

import java.util.HashMap;

public class Env {

    public HashMap<String, Symbol> tablaSimbolos;
    public Env preview, next;

    private int last_position;

    public Env(Env preview) {
        this.preview = preview;
        tablaSimbolos = new HashMap <>();
        last_position = 0;
    }
    
    public void new_symbol(String name, Symbol n){
        if (tablaSimbolos.containsKey(name.toUpperCase())){
            System.out.println("La variable " + name + " ya existe.");
        }else{
            tablaSimbolos.put(name.toUpperCase(), n);
        } 
    }
    
    public void update_symbol(String name, Symbol n){
        Env actual = this;
        while(actual!=null){
            if (actual.tablaSimbolos.containsKey(name.toUpperCase())){
                actual.tablaSimbolos.replace(name.toUpperCase(), n);
            }
            actual = actual.preview;
        }
    }
    
    public Symbol search(String key){
        Env actual = this;
        while(actual!=null){
            if (actual.tablaSimbolos.containsKey(key.toUpperCase())){
                return actual.tablaSimbolos.get(key.toUpperCase());
            }
            actual = actual.preview;
        }
        actual = this.next;
        while(actual!=null){
            if (actual.tablaSimbolos.containsKey(key.toUpperCase())){
                return actual.tablaSimbolos.get(key.toUpperCase());
            }
            actual = actual.next;
        }
        return null;
    }
    
    public boolean exist(String key){
        Env actual = this;
        while(actual!=null){
            if (actual.tablaSimbolos.containsKey(key.toUpperCase())){
                return true;
            }
            actual = actual.preview;
        }
        actual = this.next;
        while(actual!=null){
            if (actual.tablaSimbolos.containsKey(key.toUpperCase())){
                return true;
            }
            actual = actual.next;
        }
        return false;
    }

    public int getPreviewSize(){
        int size = 0;
        Env actual = this.preview;
        while(actual!=null){
            size += actual.tablaSimbolos.size();
            actual = actual.preview;
        }
        return  size;
    }

    public String getSymolTable(){
        StringBuilder tokens = new StringBuilder("""
                <!DOCTYPE html>
                    <html lang="en">
                        <head>
                             <meta charset="UTF-8">
                             <meta http-equiv="X-UA-Compatible" content="IE=edge">
                             <meta name="viewport" content="width=device-width, initial-scale=1.0">
                             <link rel="stylesheet" href="style.css">
                             <title>Reporte</title>
                        </head>
                        <body>
                            <div class="container-table">
                                <div class="table__title1">Tabla de Simbolos</div>
                            </div>""");
        tokens.append("""
                            <div id="main-container">
                                <table>
                                    <thead>
                                        <tr>
                                        <th style="border-top-left-radius: 20px;">No.</th>
                                        <th>ID</th>
                                        <th>Tipo</th>
                                        <th>Valor</th>
                                        <th>Fila</th>
                                        <th style="border-top-right-radius: 20px;">Columna</th>
                                        </tr>
                                    </thead>""");
        int i = 1;
        Env actual = this;
        while (actual!=null){
            for (Symbol s : actual.tablaSimbolos.values()) {
                tokens.append(s.html(i));
                i++;
            }
            actual = actual.preview;
        }
        actual = this.next;
        while (actual!=null){
            for (Symbol s : actual.tablaSimbolos.values()) {
                tokens.append(s.html(i));
                i++;
            }
            actual = actual.next;
        }
        tokens.append("</table></div></body></html>");

        return String.valueOf(tokens);
    }

    public int getLast_position() {
        return last_position;
    }
    public void setLast_position(int last_position) {
        this.last_position = last_position;
    }

}
