grammar gramatica;

options { caseInsensitive = true; }
// producciones

start : sentencias <EOF>;

sentencias : sentencia (sentencia)*
           ;

sentencia : main
          | subrutina
          | funcion
          | declaracion
          | sentencia_if
          | sentencia_do
          | sentencia_while
          | darreglo
          | dmatriz
          | dinamico
          | asignacion
          | callback
          | print;

main : PROGRAM id1=ID 'implicit' 'none' sentencias END PROGRAM id2=ID ;

funcion : 'function' id1=ID '(' params ')' 'result' '(' res=exp ')' 'implicit' 'none' decparams sentencias 'end' 'function' id2=ID;

subrutina : 'subroutine' id1=ID '(' params ')' 'implicit' 'none' decparams sentencias 'end' 'subroutine' id2=ID;

params: exp(',' exp)* ;

decparams: initparams+ ;

initparams : declaracion | darreglo | dmatriz ;

callback : 'call' ID '(' params ')';

print : 'print' '*' ',' exp (',' exp)*;

sentencia_do : 'do' asignacion ',' sup = exp ',' asc=exp sentencias 'end' 'do';

sentencia_while : 'do' 'while' '(' val = exp ')' sentencias 'end' 'do';

sentencia_if : 'if' '(' exp ')' 'then' sentencias (salida)* (sen_elseif)* (sen_else)* 'end' 'if';

sen_elseif : 'else' 'if' '(' exp ')' 'then' sentencias (salida)*;

sen_else: 'else' sentencias (salida)*;

salida : 'exit' | 'cycle' ;

dinamico : tipo ',' 'allocatable' ':'':' ID '('':'')'           #dinamicoV
         | tipo ',' 'allocatable' ':'':' ID '(' ':' ',' ':' ')' #dinamicoM
         ;
dmatriz : tipo ',' 'dimension' '(' x=exp ',' y=exp ')' ':'':' ID   
        | tipo (', intent(in)')? ':'':' ID '(' x=exp ',' y=exp ')'
        ;

darreglo : tipo ',' 'dimension' '(' exp ')' ':'':' ID   
         | tipo (', intent(in)')? ':'':' ID '(' exp ')'
         ;
asignacion : ID '=' exp #Avariable
            | ID '[' index = exp ']' '=' valor = exp #Avector
            | ID '=' '(''/' exp (',' exp)* '/'')' #Avlista
            | ID '[' x=exp ',' y=exp ']' '=' valor = exp #Amatriz
            | 'allocate' '(' ID '(' exp ')' ')' #Tvector
            | 'allocate' '(' ID '(' x=exp ',' y=exp ')' ')' #Tmatriz
            | 'deallocate' '(' ID ')'                       #resetD
;


declaracion : tipo (', intent(in)')? ':'':' ldeclaraciones ;

ldeclaraciones :  ID ',' ldeclaraciones         #Dindv  
                | ID '=' exp ',' ldeclaraciones #Dvalor
                | ID '=' exp                    #Dvalor2
                | ID                            #Dindv2
                ;                          

exp : op='-'   right=exp         #opExpMenos
    | left=exp op='**' right=exp  #opExp
    | left=exp op='*' right=exp  #opExp
    | left=exp op='/' right=exp  #opExp
    | left=exp op='+' right=exp  #opExp
    | left=exp op='-' right=exp  #opExp
    | left=exp op=('==' | '.eq.' | '/=' | '.ne.' | '>' | '.gt.' | '<' | '.lt.' | '>=' | '.ge.' | '<=' | '.le.') right=exp #opExpr
    | left=exp op=('.and.' | '.or.') right=exp  #opExpl
    | op='.not.' right=exp        #opExpNot
    | '(' exp ')'                #parenExp
    | ID             #literalID
    | ID '[' exp ']' #literalArr
    | ID '[' x=exp ',' y=exp ']' #literalMtx
    | ID '(' params ')'  #callfunction
    | 'size' '(' ID ')' #valorArr
    | INT            #literalINT
    | DECIMAL        #literalDEC
    | BOOL           #literalBOL
    | CHAR           #literalCHR
    | CADENA         #literalSTR
    ;

tipo : INTEGER
     | REAL 
     | COMPLEX
     | CHARACTER 
     | LOGICAL ;


// tokens
WS : [ \t\r\n]+ -> channel(HIDDEN);
COMMENT : '!' ~[\r\n]* '\r'? '\n' -> skip ;

PROGRAM : 'program' ;
END : 'end' ;
INTEGER : 'integer';
REAL : 'real';
COMPLEX : 'complex';
CHARACTER : 'character';
LOGICAL : 'logical';

ID : ([a-zA-Z_])[a-zA-Z0-9_ñÑ]* ;

INT : [0-9]+ ;
DECIMAL : [0-9]+'.'[0-9]+ ;
BOOL : ('.true.' | '.false.') ;
CHAR : ('"'|'\'') (~["\r\n] | '""')? ('"'|'\'') ;
CADENA : ('"'|'\'') (~["\r\n] | '""')* ('"'|'\'') ;



