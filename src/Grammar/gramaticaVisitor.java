// Generated from gramatica.g4 by ANTLR 4.10.1
package Grammar;
import org.antlr.v4.runtime.tree.ParseTreeVisitor;

/**
 * This interface defines a complete generic visitor for a parse tree produced
 * by {@link gramaticaParser}.
 *
 * @param <T> The return type of the visit operation. Use {@link Void} for
 * operations with no return type.
 */
public interface gramaticaVisitor<T> extends ParseTreeVisitor<T> {
	/**
	 * Visit a parse tree produced by {@link gramaticaParser#start}.
	 * @param ctx the parse tree
	 * @return the visitor result
	 */
	T visitStart(gramaticaParser.StartContext ctx);
	/**
	 * Visit a parse tree produced by {@link gramaticaParser#sentencias}.
	 * @param ctx the parse tree
	 * @return the visitor result
	 */
	T visitSentencias(gramaticaParser.SentenciasContext ctx);
	/**
	 * Visit a parse tree produced by {@link gramaticaParser#sentencia}.
	 * @param ctx the parse tree
	 * @return the visitor result
	 */
	T visitSentencia(gramaticaParser.SentenciaContext ctx);
	/**
	 * Visit a parse tree produced by {@link gramaticaParser#main}.
	 * @param ctx the parse tree
	 * @return the visitor result
	 */
	T visitMain(gramaticaParser.MainContext ctx);
	/**
	 * Visit a parse tree produced by {@link gramaticaParser#funcion}.
	 * @param ctx the parse tree
	 * @return the visitor result
	 */
	T visitFuncion(gramaticaParser.FuncionContext ctx);
	/**
	 * Visit a parse tree produced by {@link gramaticaParser#subrutina}.
	 * @param ctx the parse tree
	 * @return the visitor result
	 */
	T visitSubrutina(gramaticaParser.SubrutinaContext ctx);
	/**
	 * Visit a parse tree produced by {@link gramaticaParser#params}.
	 * @param ctx the parse tree
	 * @return the visitor result
	 */
	T visitParams(gramaticaParser.ParamsContext ctx);
	/**
	 * Visit a parse tree produced by {@link gramaticaParser#decparams}.
	 * @param ctx the parse tree
	 * @return the visitor result
	 */
	T visitDecparams(gramaticaParser.DecparamsContext ctx);
	/**
	 * Visit a parse tree produced by {@link gramaticaParser#initparams}.
	 * @param ctx the parse tree
	 * @return the visitor result
	 */
	T visitInitparams(gramaticaParser.InitparamsContext ctx);
	/**
	 * Visit a parse tree produced by {@link gramaticaParser#callback}.
	 * @param ctx the parse tree
	 * @return the visitor result
	 */
	T visitCallback(gramaticaParser.CallbackContext ctx);
	/**
	 * Visit a parse tree produced by {@link gramaticaParser#print}.
	 * @param ctx the parse tree
	 * @return the visitor result
	 */
	T visitPrint(gramaticaParser.PrintContext ctx);
	/**
	 * Visit a parse tree produced by {@link gramaticaParser#sentencia_do}.
	 * @param ctx the parse tree
	 * @return the visitor result
	 */
	T visitSentencia_do(gramaticaParser.Sentencia_doContext ctx);
	/**
	 * Visit a parse tree produced by {@link gramaticaParser#sentencia_while}.
	 * @param ctx the parse tree
	 * @return the visitor result
	 */
	T visitSentencia_while(gramaticaParser.Sentencia_whileContext ctx);
	/**
	 * Visit a parse tree produced by {@link gramaticaParser#sentencia_if}.
	 * @param ctx the parse tree
	 * @return the visitor result
	 */
	T visitSentencia_if(gramaticaParser.Sentencia_ifContext ctx);
	/**
	 * Visit a parse tree produced by {@link gramaticaParser#sen_elseif}.
	 * @param ctx the parse tree
	 * @return the visitor result
	 */
	T visitSen_elseif(gramaticaParser.Sen_elseifContext ctx);
	/**
	 * Visit a parse tree produced by {@link gramaticaParser#sen_else}.
	 * @param ctx the parse tree
	 * @return the visitor result
	 */
	T visitSen_else(gramaticaParser.Sen_elseContext ctx);
	/**
	 * Visit a parse tree produced by {@link gramaticaParser#salida}.
	 * @param ctx the parse tree
	 * @return the visitor result
	 */
	T visitSalida(gramaticaParser.SalidaContext ctx);
	/**
	 * Visit a parse tree produced by the {@code dinamicoV}
	 * labeled alternative in {@link gramaticaParser#dinamico}.
	 * @param ctx the parse tree
	 * @return the visitor result
	 */
	T visitDinamicoV(gramaticaParser.DinamicoVContext ctx);
	/**
	 * Visit a parse tree produced by the {@code dinamicoM}
	 * labeled alternative in {@link gramaticaParser#dinamico}.
	 * @param ctx the parse tree
	 * @return the visitor result
	 */
	T visitDinamicoM(gramaticaParser.DinamicoMContext ctx);
	/**
	 * Visit a parse tree produced by {@link gramaticaParser#dmatriz}.
	 * @param ctx the parse tree
	 * @return the visitor result
	 */
	T visitDmatriz(gramaticaParser.DmatrizContext ctx);
	/**
	 * Visit a parse tree produced by {@link gramaticaParser#darreglo}.
	 * @param ctx the parse tree
	 * @return the visitor result
	 */
	T visitDarreglo(gramaticaParser.DarregloContext ctx);
	/**
	 * Visit a parse tree produced by the {@code Avariable}
	 * labeled alternative in {@link gramaticaParser#asignacion}.
	 * @param ctx the parse tree
	 * @return the visitor result
	 */
	T visitAvariable(gramaticaParser.AvariableContext ctx);
	/**
	 * Visit a parse tree produced by the {@code Avector}
	 * labeled alternative in {@link gramaticaParser#asignacion}.
	 * @param ctx the parse tree
	 * @return the visitor result
	 */
	T visitAvector(gramaticaParser.AvectorContext ctx);
	/**
	 * Visit a parse tree produced by the {@code Avlista}
	 * labeled alternative in {@link gramaticaParser#asignacion}.
	 * @param ctx the parse tree
	 * @return the visitor result
	 */
	T visitAvlista(gramaticaParser.AvlistaContext ctx);
	/**
	 * Visit a parse tree produced by the {@code Amatriz}
	 * labeled alternative in {@link gramaticaParser#asignacion}.
	 * @param ctx the parse tree
	 * @return the visitor result
	 */
	T visitAmatriz(gramaticaParser.AmatrizContext ctx);
	/**
	 * Visit a parse tree produced by the {@code Tvector}
	 * labeled alternative in {@link gramaticaParser#asignacion}.
	 * @param ctx the parse tree
	 * @return the visitor result
	 */
	T visitTvector(gramaticaParser.TvectorContext ctx);
	/**
	 * Visit a parse tree produced by the {@code Tmatriz}
	 * labeled alternative in {@link gramaticaParser#asignacion}.
	 * @param ctx the parse tree
	 * @return the visitor result
	 */
	T visitTmatriz(gramaticaParser.TmatrizContext ctx);
	/**
	 * Visit a parse tree produced by the {@code resetD}
	 * labeled alternative in {@link gramaticaParser#asignacion}.
	 * @param ctx the parse tree
	 * @return the visitor result
	 */
	T visitResetD(gramaticaParser.ResetDContext ctx);
	/**
	 * Visit a parse tree produced by {@link gramaticaParser#declaracion}.
	 * @param ctx the parse tree
	 * @return the visitor result
	 */
	T visitDeclaracion(gramaticaParser.DeclaracionContext ctx);
	/**
	 * Visit a parse tree produced by the {@code Dindv}
	 * labeled alternative in {@link gramaticaParser#ldeclaraciones}.
	 * @param ctx the parse tree
	 * @return the visitor result
	 */
	T visitDindv(gramaticaParser.DindvContext ctx);
	/**
	 * Visit a parse tree produced by the {@code Dvalor}
	 * labeled alternative in {@link gramaticaParser#ldeclaraciones}.
	 * @param ctx the parse tree
	 * @return the visitor result
	 */
	T visitDvalor(gramaticaParser.DvalorContext ctx);
	/**
	 * Visit a parse tree produced by the {@code Dvalor2}
	 * labeled alternative in {@link gramaticaParser#ldeclaraciones}.
	 * @param ctx the parse tree
	 * @return the visitor result
	 */
	T visitDvalor2(gramaticaParser.Dvalor2Context ctx);
	/**
	 * Visit a parse tree produced by the {@code Dindv2}
	 * labeled alternative in {@link gramaticaParser#ldeclaraciones}.
	 * @param ctx the parse tree
	 * @return the visitor result
	 */
	T visitDindv2(gramaticaParser.Dindv2Context ctx);
	/**
	 * Visit a parse tree produced by the {@code literalCHR}
	 * labeled alternative in {@link gramaticaParser#exp}.
	 * @param ctx the parse tree
	 * @return the visitor result
	 */
	T visitLiteralCHR(gramaticaParser.LiteralCHRContext ctx);
	/**
	 * Visit a parse tree produced by the {@code opExpr}
	 * labeled alternative in {@link gramaticaParser#exp}.
	 * @param ctx the parse tree
	 * @return the visitor result
	 */
	T visitOpExpr(gramaticaParser.OpExprContext ctx);
	/**
	 * Visit a parse tree produced by the {@code opExpl}
	 * labeled alternative in {@link gramaticaParser#exp}.
	 * @param ctx the parse tree
	 * @return the visitor result
	 */
	T visitOpExpl(gramaticaParser.OpExplContext ctx);
	/**
	 * Visit a parse tree produced by the {@code literalBOL}
	 * labeled alternative in {@link gramaticaParser#exp}.
	 * @param ctx the parse tree
	 * @return the visitor result
	 */
	T visitLiteralBOL(gramaticaParser.LiteralBOLContext ctx);
	/**
	 * Visit a parse tree produced by the {@code opExp}
	 * labeled alternative in {@link gramaticaParser#exp}.
	 * @param ctx the parse tree
	 * @return the visitor result
	 */
	T visitOpExp(gramaticaParser.OpExpContext ctx);
	/**
	 * Visit a parse tree produced by the {@code opExpMenos}
	 * labeled alternative in {@link gramaticaParser#exp}.
	 * @param ctx the parse tree
	 * @return the visitor result
	 */
	T visitOpExpMenos(gramaticaParser.OpExpMenosContext ctx);
	/**
	 * Visit a parse tree produced by the {@code literalArr}
	 * labeled alternative in {@link gramaticaParser#exp}.
	 * @param ctx the parse tree
	 * @return the visitor result
	 */
	T visitLiteralArr(gramaticaParser.LiteralArrContext ctx);
	/**
	 * Visit a parse tree produced by the {@code literalDEC}
	 * labeled alternative in {@link gramaticaParser#exp}.
	 * @param ctx the parse tree
	 * @return the visitor result
	 */
	T visitLiteralDEC(gramaticaParser.LiteralDECContext ctx);
	/**
	 * Visit a parse tree produced by the {@code literalID}
	 * labeled alternative in {@link gramaticaParser#exp}.
	 * @param ctx the parse tree
	 * @return the visitor result
	 */
	T visitLiteralID(gramaticaParser.LiteralIDContext ctx);
	/**
	 * Visit a parse tree produced by the {@code callfunction}
	 * labeled alternative in {@link gramaticaParser#exp}.
	 * @param ctx the parse tree
	 * @return the visitor result
	 */
	T visitCallfunction(gramaticaParser.CallfunctionContext ctx);
	/**
	 * Visit a parse tree produced by the {@code parenExp}
	 * labeled alternative in {@link gramaticaParser#exp}.
	 * @param ctx the parse tree
	 * @return the visitor result
	 */
	T visitParenExp(gramaticaParser.ParenExpContext ctx);
	/**
	 * Visit a parse tree produced by the {@code literalINT}
	 * labeled alternative in {@link gramaticaParser#exp}.
	 * @param ctx the parse tree
	 * @return the visitor result
	 */
	T visitLiteralINT(gramaticaParser.LiteralINTContext ctx);
	/**
	 * Visit a parse tree produced by the {@code opExpNot}
	 * labeled alternative in {@link gramaticaParser#exp}.
	 * @param ctx the parse tree
	 * @return the visitor result
	 */
	T visitOpExpNot(gramaticaParser.OpExpNotContext ctx);
	/**
	 * Visit a parse tree produced by the {@code literalMtx}
	 * labeled alternative in {@link gramaticaParser#exp}.
	 * @param ctx the parse tree
	 * @return the visitor result
	 */
	T visitLiteralMtx(gramaticaParser.LiteralMtxContext ctx);
	/**
	 * Visit a parse tree produced by the {@code valorArr}
	 * labeled alternative in {@link gramaticaParser#exp}.
	 * @param ctx the parse tree
	 * @return the visitor result
	 */
	T visitValorArr(gramaticaParser.ValorArrContext ctx);
	/**
	 * Visit a parse tree produced by the {@code literalSTR}
	 * labeled alternative in {@link gramaticaParser#exp}.
	 * @param ctx the parse tree
	 * @return the visitor result
	 */
	T visitLiteralSTR(gramaticaParser.LiteralSTRContext ctx);
	/**
	 * Visit a parse tree produced by {@link gramaticaParser#tipo}.
	 * @param ctx the parse tree
	 * @return the visitor result
	 */
	T visitTipo(gramaticaParser.TipoContext ctx);
}