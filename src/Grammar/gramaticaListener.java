// Generated from gramatica.g4 by ANTLR 4.10.1
package Grammar;
import org.antlr.v4.runtime.tree.ParseTreeListener;

/**
 * This interface defines a complete listener for a parse tree produced by
 * {@link gramaticaParser}.
 */
public interface gramaticaListener extends ParseTreeListener {
	/**
	 * Enter a parse tree produced by {@link gramaticaParser#start}.
	 * @param ctx the parse tree
	 */
	void enterStart(gramaticaParser.StartContext ctx);
	/**
	 * Exit a parse tree produced by {@link gramaticaParser#start}.
	 * @param ctx the parse tree
	 */
	void exitStart(gramaticaParser.StartContext ctx);
	/**
	 * Enter a parse tree produced by {@link gramaticaParser#sentencias}.
	 * @param ctx the parse tree
	 */
	void enterSentencias(gramaticaParser.SentenciasContext ctx);
	/**
	 * Exit a parse tree produced by {@link gramaticaParser#sentencias}.
	 * @param ctx the parse tree
	 */
	void exitSentencias(gramaticaParser.SentenciasContext ctx);
	/**
	 * Enter a parse tree produced by {@link gramaticaParser#sentencia}.
	 * @param ctx the parse tree
	 */
	void enterSentencia(gramaticaParser.SentenciaContext ctx);
	/**
	 * Exit a parse tree produced by {@link gramaticaParser#sentencia}.
	 * @param ctx the parse tree
	 */
	void exitSentencia(gramaticaParser.SentenciaContext ctx);
	/**
	 * Enter a parse tree produced by {@link gramaticaParser#main}.
	 * @param ctx the parse tree
	 */
	void enterMain(gramaticaParser.MainContext ctx);
	/**
	 * Exit a parse tree produced by {@link gramaticaParser#main}.
	 * @param ctx the parse tree
	 */
	void exitMain(gramaticaParser.MainContext ctx);
	/**
	 * Enter a parse tree produced by {@link gramaticaParser#funcion}.
	 * @param ctx the parse tree
	 */
	void enterFuncion(gramaticaParser.FuncionContext ctx);
	/**
	 * Exit a parse tree produced by {@link gramaticaParser#funcion}.
	 * @param ctx the parse tree
	 */
	void exitFuncion(gramaticaParser.FuncionContext ctx);
	/**
	 * Enter a parse tree produced by {@link gramaticaParser#subrutina}.
	 * @param ctx the parse tree
	 */
	void enterSubrutina(gramaticaParser.SubrutinaContext ctx);
	/**
	 * Exit a parse tree produced by {@link gramaticaParser#subrutina}.
	 * @param ctx the parse tree
	 */
	void exitSubrutina(gramaticaParser.SubrutinaContext ctx);
	/**
	 * Enter a parse tree produced by {@link gramaticaParser#params}.
	 * @param ctx the parse tree
	 */
	void enterParams(gramaticaParser.ParamsContext ctx);
	/**
	 * Exit a parse tree produced by {@link gramaticaParser#params}.
	 * @param ctx the parse tree
	 */
	void exitParams(gramaticaParser.ParamsContext ctx);
	/**
	 * Enter a parse tree produced by {@link gramaticaParser#decparams}.
	 * @param ctx the parse tree
	 */
	void enterDecparams(gramaticaParser.DecparamsContext ctx);
	/**
	 * Exit a parse tree produced by {@link gramaticaParser#decparams}.
	 * @param ctx the parse tree
	 */
	void exitDecparams(gramaticaParser.DecparamsContext ctx);
	/**
	 * Enter a parse tree produced by {@link gramaticaParser#initparams}.
	 * @param ctx the parse tree
	 */
	void enterInitparams(gramaticaParser.InitparamsContext ctx);
	/**
	 * Exit a parse tree produced by {@link gramaticaParser#initparams}.
	 * @param ctx the parse tree
	 */
	void exitInitparams(gramaticaParser.InitparamsContext ctx);
	/**
	 * Enter a parse tree produced by {@link gramaticaParser#callback}.
	 * @param ctx the parse tree
	 */
	void enterCallback(gramaticaParser.CallbackContext ctx);
	/**
	 * Exit a parse tree produced by {@link gramaticaParser#callback}.
	 * @param ctx the parse tree
	 */
	void exitCallback(gramaticaParser.CallbackContext ctx);
	/**
	 * Enter a parse tree produced by {@link gramaticaParser#print}.
	 * @param ctx the parse tree
	 */
	void enterPrint(gramaticaParser.PrintContext ctx);
	/**
	 * Exit a parse tree produced by {@link gramaticaParser#print}.
	 * @param ctx the parse tree
	 */
	void exitPrint(gramaticaParser.PrintContext ctx);
	/**
	 * Enter a parse tree produced by {@link gramaticaParser#sentencia_do}.
	 * @param ctx the parse tree
	 */
	void enterSentencia_do(gramaticaParser.Sentencia_doContext ctx);
	/**
	 * Exit a parse tree produced by {@link gramaticaParser#sentencia_do}.
	 * @param ctx the parse tree
	 */
	void exitSentencia_do(gramaticaParser.Sentencia_doContext ctx);
	/**
	 * Enter a parse tree produced by {@link gramaticaParser#sentencia_while}.
	 * @param ctx the parse tree
	 */
	void enterSentencia_while(gramaticaParser.Sentencia_whileContext ctx);
	/**
	 * Exit a parse tree produced by {@link gramaticaParser#sentencia_while}.
	 * @param ctx the parse tree
	 */
	void exitSentencia_while(gramaticaParser.Sentencia_whileContext ctx);
	/**
	 * Enter a parse tree produced by {@link gramaticaParser#sentencia_if}.
	 * @param ctx the parse tree
	 */
	void enterSentencia_if(gramaticaParser.Sentencia_ifContext ctx);
	/**
	 * Exit a parse tree produced by {@link gramaticaParser#sentencia_if}.
	 * @param ctx the parse tree
	 */
	void exitSentencia_if(gramaticaParser.Sentencia_ifContext ctx);
	/**
	 * Enter a parse tree produced by {@link gramaticaParser#sen_elseif}.
	 * @param ctx the parse tree
	 */
	void enterSen_elseif(gramaticaParser.Sen_elseifContext ctx);
	/**
	 * Exit a parse tree produced by {@link gramaticaParser#sen_elseif}.
	 * @param ctx the parse tree
	 */
	void exitSen_elseif(gramaticaParser.Sen_elseifContext ctx);
	/**
	 * Enter a parse tree produced by {@link gramaticaParser#sen_else}.
	 * @param ctx the parse tree
	 */
	void enterSen_else(gramaticaParser.Sen_elseContext ctx);
	/**
	 * Exit a parse tree produced by {@link gramaticaParser#sen_else}.
	 * @param ctx the parse tree
	 */
	void exitSen_else(gramaticaParser.Sen_elseContext ctx);
	/**
	 * Enter a parse tree produced by {@link gramaticaParser#salida}.
	 * @param ctx the parse tree
	 */
	void enterSalida(gramaticaParser.SalidaContext ctx);
	/**
	 * Exit a parse tree produced by {@link gramaticaParser#salida}.
	 * @param ctx the parse tree
	 */
	void exitSalida(gramaticaParser.SalidaContext ctx);
	/**
	 * Enter a parse tree produced by the {@code dinamicoV}
	 * labeled alternative in {@link gramaticaParser#dinamico}.
	 * @param ctx the parse tree
	 */
	void enterDinamicoV(gramaticaParser.DinamicoVContext ctx);
	/**
	 * Exit a parse tree produced by the {@code dinamicoV}
	 * labeled alternative in {@link gramaticaParser#dinamico}.
	 * @param ctx the parse tree
	 */
	void exitDinamicoV(gramaticaParser.DinamicoVContext ctx);
	/**
	 * Enter a parse tree produced by the {@code dinamicoM}
	 * labeled alternative in {@link gramaticaParser#dinamico}.
	 * @param ctx the parse tree
	 */
	void enterDinamicoM(gramaticaParser.DinamicoMContext ctx);
	/**
	 * Exit a parse tree produced by the {@code dinamicoM}
	 * labeled alternative in {@link gramaticaParser#dinamico}.
	 * @param ctx the parse tree
	 */
	void exitDinamicoM(gramaticaParser.DinamicoMContext ctx);
	/**
	 * Enter a parse tree produced by {@link gramaticaParser#dmatriz}.
	 * @param ctx the parse tree
	 */
	void enterDmatriz(gramaticaParser.DmatrizContext ctx);
	/**
	 * Exit a parse tree produced by {@link gramaticaParser#dmatriz}.
	 * @param ctx the parse tree
	 */
	void exitDmatriz(gramaticaParser.DmatrizContext ctx);
	/**
	 * Enter a parse tree produced by {@link gramaticaParser#darreglo}.
	 * @param ctx the parse tree
	 */
	void enterDarreglo(gramaticaParser.DarregloContext ctx);
	/**
	 * Exit a parse tree produced by {@link gramaticaParser#darreglo}.
	 * @param ctx the parse tree
	 */
	void exitDarreglo(gramaticaParser.DarregloContext ctx);
	/**
	 * Enter a parse tree produced by the {@code Avariable}
	 * labeled alternative in {@link gramaticaParser#asignacion}.
	 * @param ctx the parse tree
	 */
	void enterAvariable(gramaticaParser.AvariableContext ctx);
	/**
	 * Exit a parse tree produced by the {@code Avariable}
	 * labeled alternative in {@link gramaticaParser#asignacion}.
	 * @param ctx the parse tree
	 */
	void exitAvariable(gramaticaParser.AvariableContext ctx);
	/**
	 * Enter a parse tree produced by the {@code Avector}
	 * labeled alternative in {@link gramaticaParser#asignacion}.
	 * @param ctx the parse tree
	 */
	void enterAvector(gramaticaParser.AvectorContext ctx);
	/**
	 * Exit a parse tree produced by the {@code Avector}
	 * labeled alternative in {@link gramaticaParser#asignacion}.
	 * @param ctx the parse tree
	 */
	void exitAvector(gramaticaParser.AvectorContext ctx);
	/**
	 * Enter a parse tree produced by the {@code Avlista}
	 * labeled alternative in {@link gramaticaParser#asignacion}.
	 * @param ctx the parse tree
	 */
	void enterAvlista(gramaticaParser.AvlistaContext ctx);
	/**
	 * Exit a parse tree produced by the {@code Avlista}
	 * labeled alternative in {@link gramaticaParser#asignacion}.
	 * @param ctx the parse tree
	 */
	void exitAvlista(gramaticaParser.AvlistaContext ctx);
	/**
	 * Enter a parse tree produced by the {@code Amatriz}
	 * labeled alternative in {@link gramaticaParser#asignacion}.
	 * @param ctx the parse tree
	 */
	void enterAmatriz(gramaticaParser.AmatrizContext ctx);
	/**
	 * Exit a parse tree produced by the {@code Amatriz}
	 * labeled alternative in {@link gramaticaParser#asignacion}.
	 * @param ctx the parse tree
	 */
	void exitAmatriz(gramaticaParser.AmatrizContext ctx);
	/**
	 * Enter a parse tree produced by the {@code Tvector}
	 * labeled alternative in {@link gramaticaParser#asignacion}.
	 * @param ctx the parse tree
	 */
	void enterTvector(gramaticaParser.TvectorContext ctx);
	/**
	 * Exit a parse tree produced by the {@code Tvector}
	 * labeled alternative in {@link gramaticaParser#asignacion}.
	 * @param ctx the parse tree
	 */
	void exitTvector(gramaticaParser.TvectorContext ctx);
	/**
	 * Enter a parse tree produced by the {@code Tmatriz}
	 * labeled alternative in {@link gramaticaParser#asignacion}.
	 * @param ctx the parse tree
	 */
	void enterTmatriz(gramaticaParser.TmatrizContext ctx);
	/**
	 * Exit a parse tree produced by the {@code Tmatriz}
	 * labeled alternative in {@link gramaticaParser#asignacion}.
	 * @param ctx the parse tree
	 */
	void exitTmatriz(gramaticaParser.TmatrizContext ctx);
	/**
	 * Enter a parse tree produced by the {@code resetD}
	 * labeled alternative in {@link gramaticaParser#asignacion}.
	 * @param ctx the parse tree
	 */
	void enterResetD(gramaticaParser.ResetDContext ctx);
	/**
	 * Exit a parse tree produced by the {@code resetD}
	 * labeled alternative in {@link gramaticaParser#asignacion}.
	 * @param ctx the parse tree
	 */
	void exitResetD(gramaticaParser.ResetDContext ctx);
	/**
	 * Enter a parse tree produced by {@link gramaticaParser#declaracion}.
	 * @param ctx the parse tree
	 */
	void enterDeclaracion(gramaticaParser.DeclaracionContext ctx);
	/**
	 * Exit a parse tree produced by {@link gramaticaParser#declaracion}.
	 * @param ctx the parse tree
	 */
	void exitDeclaracion(gramaticaParser.DeclaracionContext ctx);
	/**
	 * Enter a parse tree produced by the {@code Dindv}
	 * labeled alternative in {@link gramaticaParser#ldeclaraciones}.
	 * @param ctx the parse tree
	 */
	void enterDindv(gramaticaParser.DindvContext ctx);
	/**
	 * Exit a parse tree produced by the {@code Dindv}
	 * labeled alternative in {@link gramaticaParser#ldeclaraciones}.
	 * @param ctx the parse tree
	 */
	void exitDindv(gramaticaParser.DindvContext ctx);
	/**
	 * Enter a parse tree produced by the {@code Dvalor}
	 * labeled alternative in {@link gramaticaParser#ldeclaraciones}.
	 * @param ctx the parse tree
	 */
	void enterDvalor(gramaticaParser.DvalorContext ctx);
	/**
	 * Exit a parse tree produced by the {@code Dvalor}
	 * labeled alternative in {@link gramaticaParser#ldeclaraciones}.
	 * @param ctx the parse tree
	 */
	void exitDvalor(gramaticaParser.DvalorContext ctx);
	/**
	 * Enter a parse tree produced by the {@code Dvalor2}
	 * labeled alternative in {@link gramaticaParser#ldeclaraciones}.
	 * @param ctx the parse tree
	 */
	void enterDvalor2(gramaticaParser.Dvalor2Context ctx);
	/**
	 * Exit a parse tree produced by the {@code Dvalor2}
	 * labeled alternative in {@link gramaticaParser#ldeclaraciones}.
	 * @param ctx the parse tree
	 */
	void exitDvalor2(gramaticaParser.Dvalor2Context ctx);
	/**
	 * Enter a parse tree produced by the {@code Dindv2}
	 * labeled alternative in {@link gramaticaParser#ldeclaraciones}.
	 * @param ctx the parse tree
	 */
	void enterDindv2(gramaticaParser.Dindv2Context ctx);
	/**
	 * Exit a parse tree produced by the {@code Dindv2}
	 * labeled alternative in {@link gramaticaParser#ldeclaraciones}.
	 * @param ctx the parse tree
	 */
	void exitDindv2(gramaticaParser.Dindv2Context ctx);
	/**
	 * Enter a parse tree produced by the {@code literalCHR}
	 * labeled alternative in {@link gramaticaParser#exp}.
	 * @param ctx the parse tree
	 */
	void enterLiteralCHR(gramaticaParser.LiteralCHRContext ctx);
	/**
	 * Exit a parse tree produced by the {@code literalCHR}
	 * labeled alternative in {@link gramaticaParser#exp}.
	 * @param ctx the parse tree
	 */
	void exitLiteralCHR(gramaticaParser.LiteralCHRContext ctx);
	/**
	 * Enter a parse tree produced by the {@code opExpr}
	 * labeled alternative in {@link gramaticaParser#exp}.
	 * @param ctx the parse tree
	 */
	void enterOpExpr(gramaticaParser.OpExprContext ctx);
	/**
	 * Exit a parse tree produced by the {@code opExpr}
	 * labeled alternative in {@link gramaticaParser#exp}.
	 * @param ctx the parse tree
	 */
	void exitOpExpr(gramaticaParser.OpExprContext ctx);
	/**
	 * Enter a parse tree produced by the {@code opExpl}
	 * labeled alternative in {@link gramaticaParser#exp}.
	 * @param ctx the parse tree
	 */
	void enterOpExpl(gramaticaParser.OpExplContext ctx);
	/**
	 * Exit a parse tree produced by the {@code opExpl}
	 * labeled alternative in {@link gramaticaParser#exp}.
	 * @param ctx the parse tree
	 */
	void exitOpExpl(gramaticaParser.OpExplContext ctx);
	/**
	 * Enter a parse tree produced by the {@code literalBOL}
	 * labeled alternative in {@link gramaticaParser#exp}.
	 * @param ctx the parse tree
	 */
	void enterLiteralBOL(gramaticaParser.LiteralBOLContext ctx);
	/**
	 * Exit a parse tree produced by the {@code literalBOL}
	 * labeled alternative in {@link gramaticaParser#exp}.
	 * @param ctx the parse tree
	 */
	void exitLiteralBOL(gramaticaParser.LiteralBOLContext ctx);
	/**
	 * Enter a parse tree produced by the {@code opExp}
	 * labeled alternative in {@link gramaticaParser#exp}.
	 * @param ctx the parse tree
	 */
	void enterOpExp(gramaticaParser.OpExpContext ctx);
	/**
	 * Exit a parse tree produced by the {@code opExp}
	 * labeled alternative in {@link gramaticaParser#exp}.
	 * @param ctx the parse tree
	 */
	void exitOpExp(gramaticaParser.OpExpContext ctx);
	/**
	 * Enter a parse tree produced by the {@code opExpMenos}
	 * labeled alternative in {@link gramaticaParser#exp}.
	 * @param ctx the parse tree
	 */
	void enterOpExpMenos(gramaticaParser.OpExpMenosContext ctx);
	/**
	 * Exit a parse tree produced by the {@code opExpMenos}
	 * labeled alternative in {@link gramaticaParser#exp}.
	 * @param ctx the parse tree
	 */
	void exitOpExpMenos(gramaticaParser.OpExpMenosContext ctx);
	/**
	 * Enter a parse tree produced by the {@code literalArr}
	 * labeled alternative in {@link gramaticaParser#exp}.
	 * @param ctx the parse tree
	 */
	void enterLiteralArr(gramaticaParser.LiteralArrContext ctx);
	/**
	 * Exit a parse tree produced by the {@code literalArr}
	 * labeled alternative in {@link gramaticaParser#exp}.
	 * @param ctx the parse tree
	 */
	void exitLiteralArr(gramaticaParser.LiteralArrContext ctx);
	/**
	 * Enter a parse tree produced by the {@code literalDEC}
	 * labeled alternative in {@link gramaticaParser#exp}.
	 * @param ctx the parse tree
	 */
	void enterLiteralDEC(gramaticaParser.LiteralDECContext ctx);
	/**
	 * Exit a parse tree produced by the {@code literalDEC}
	 * labeled alternative in {@link gramaticaParser#exp}.
	 * @param ctx the parse tree
	 */
	void exitLiteralDEC(gramaticaParser.LiteralDECContext ctx);
	/**
	 * Enter a parse tree produced by the {@code literalID}
	 * labeled alternative in {@link gramaticaParser#exp}.
	 * @param ctx the parse tree
	 */
	void enterLiteralID(gramaticaParser.LiteralIDContext ctx);
	/**
	 * Exit a parse tree produced by the {@code literalID}
	 * labeled alternative in {@link gramaticaParser#exp}.
	 * @param ctx the parse tree
	 */
	void exitLiteralID(gramaticaParser.LiteralIDContext ctx);
	/**
	 * Enter a parse tree produced by the {@code callfunction}
	 * labeled alternative in {@link gramaticaParser#exp}.
	 * @param ctx the parse tree
	 */
	void enterCallfunction(gramaticaParser.CallfunctionContext ctx);
	/**
	 * Exit a parse tree produced by the {@code callfunction}
	 * labeled alternative in {@link gramaticaParser#exp}.
	 * @param ctx the parse tree
	 */
	void exitCallfunction(gramaticaParser.CallfunctionContext ctx);
	/**
	 * Enter a parse tree produced by the {@code parenExp}
	 * labeled alternative in {@link gramaticaParser#exp}.
	 * @param ctx the parse tree
	 */
	void enterParenExp(gramaticaParser.ParenExpContext ctx);
	/**
	 * Exit a parse tree produced by the {@code parenExp}
	 * labeled alternative in {@link gramaticaParser#exp}.
	 * @param ctx the parse tree
	 */
	void exitParenExp(gramaticaParser.ParenExpContext ctx);
	/**
	 * Enter a parse tree produced by the {@code literalINT}
	 * labeled alternative in {@link gramaticaParser#exp}.
	 * @param ctx the parse tree
	 */
	void enterLiteralINT(gramaticaParser.LiteralINTContext ctx);
	/**
	 * Exit a parse tree produced by the {@code literalINT}
	 * labeled alternative in {@link gramaticaParser#exp}.
	 * @param ctx the parse tree
	 */
	void exitLiteralINT(gramaticaParser.LiteralINTContext ctx);
	/**
	 * Enter a parse tree produced by the {@code opExpNot}
	 * labeled alternative in {@link gramaticaParser#exp}.
	 * @param ctx the parse tree
	 */
	void enterOpExpNot(gramaticaParser.OpExpNotContext ctx);
	/**
	 * Exit a parse tree produced by the {@code opExpNot}
	 * labeled alternative in {@link gramaticaParser#exp}.
	 * @param ctx the parse tree
	 */
	void exitOpExpNot(gramaticaParser.OpExpNotContext ctx);
	/**
	 * Enter a parse tree produced by the {@code literalMtx}
	 * labeled alternative in {@link gramaticaParser#exp}.
	 * @param ctx the parse tree
	 */
	void enterLiteralMtx(gramaticaParser.LiteralMtxContext ctx);
	/**
	 * Exit a parse tree produced by the {@code literalMtx}
	 * labeled alternative in {@link gramaticaParser#exp}.
	 * @param ctx the parse tree
	 */
	void exitLiteralMtx(gramaticaParser.LiteralMtxContext ctx);
	/**
	 * Enter a parse tree produced by the {@code valorArr}
	 * labeled alternative in {@link gramaticaParser#exp}.
	 * @param ctx the parse tree
	 */
	void enterValorArr(gramaticaParser.ValorArrContext ctx);
	/**
	 * Exit a parse tree produced by the {@code valorArr}
	 * labeled alternative in {@link gramaticaParser#exp}.
	 * @param ctx the parse tree
	 */
	void exitValorArr(gramaticaParser.ValorArrContext ctx);
	/**
	 * Enter a parse tree produced by the {@code literalSTR}
	 * labeled alternative in {@link gramaticaParser#exp}.
	 * @param ctx the parse tree
	 */
	void enterLiteralSTR(gramaticaParser.LiteralSTRContext ctx);
	/**
	 * Exit a parse tree produced by the {@code literalSTR}
	 * labeled alternative in {@link gramaticaParser#exp}.
	 * @param ctx the parse tree
	 */
	void exitLiteralSTR(gramaticaParser.LiteralSTRContext ctx);
	/**
	 * Enter a parse tree produced by {@link gramaticaParser#tipo}.
	 * @param ctx the parse tree
	 */
	void enterTipo(gramaticaParser.TipoContext ctx);
	/**
	 * Exit a parse tree produced by {@link gramaticaParser#tipo}.
	 * @param ctx the parse tree
	 */
	void exitTipo(gramaticaParser.TipoContext ctx);
}